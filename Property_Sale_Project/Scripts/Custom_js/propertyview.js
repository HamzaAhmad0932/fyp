﻿///// <reference path="../angular.js" />
(function () {
    'use strict';
    angular.module("MainApp.propertyview", ['datatables']);

    angular
        .module("MainApp.propertyview")
        .controller("PropertyViewListController", ['$scope', 'propertyviewService', function ($scope, propertyviewService) {
            var vm = this;
            vm.properties;

            ////
            activate();

            function activate() {
                propertyviewService.fetchAllProperties(function (data) {
                    vm.properties = data;
                });
                
            }
        }]);

    /////////////////////end controller///////////////////////////////
    angular
        .module('MainApp.propertyview')
        .service('propertyviewService', ['$http', function ($http) {
            var service = {
                fetchAllProperties: fetchAllProperties
            };
            return service;

            function fetchAllProperties(cbs) {
                $http({
                    url: 'Properties_View/getAllProperties',
                    method: 'POST',
                }).then(function (resp) {
                    //success
                    cbs(resp.data);
                }, function (resp) {
                    //unsuccess
                });
            }
        }]);

})();
//(function () {
//    $(document).ready(function () {
//        var getAllProperties = function () {
//            $.ajax({
//                url: '/Properties_View/getAllProperties',
//                method: 'POST',
//                success: function (resp) {
//                    $.each(resp, function (index, property) {
//                        $('#dataTables-example').append('<tr><td>' + property.property_id + '</td><td>' + property.project_name + '</td><td>' + property.category + '</td><td>' + property.length + 'x' + property.width + '</td><td>' + property.marla + '</td><td>' + property.covered_area + '</td><td>'+property.rate+'</td><td>'+property.amount+'</td><td>'+property.total_cost+'</td><td>'+property.total_amount+'</td></tr>');
//                    });
//                },
//                error: function (err) { }
//            });
//        }

//        getAllProperties();
//    });
//})();