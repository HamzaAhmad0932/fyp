﻿/// <reference path="../angular.js" />
(function () {
    'use strict';

    angular.module('MainApp.add_project', []);

    angular
        .module('MainApp.add_project')
        .controller('projectController', projectController);
    projectController.$inject = ['ProjectService', '$scope', '$log', '$http'];

    function projectController(ProjectService) {
        var vm = this;
        vm.fetchEdit = fetchEdit;
        vm.reset = reset;
        vm.save = save;
        activate();



        /////////////////////
        function activate() {
            fillTable();
            getMaxId();
        }
        function fillTable() {
            ProjectService.fillTable(function (data) {
                vm.populateTab = data
                //console.log(data);
            });
        };
        function getMaxId() {
            ProjectService.getMaxId(function (_id) {
                //console.log(_id); !=
                vm.project_id = _id;
            });
        };
        function fetchEdit(_id) {
            if (_id.project.project_id != 0) {
                //console.log(_id.project.project_id);
                var id = (_id.project.project_id == undefined) ? 0 : _id.project.project_id;
                ProjectService.fetchEdit(id, function (data) {
                    //cbs
                    vm.hiddenId = data.project_id;
                    vm.project_name = data.project_name;
                    vm.description = data.description;
                    //console.log(data);
                }, function () {
                    //cbf
                    alert('Data Not Found..');           
                    reset();
                });
            }
        }
        function reset() {
            getMaxId();
            vm.hiddenId = '';
            vm.project_name = '';
            vm.description = '';

        }
        function save() {
            var data = {};
            data.project_id = vm.hiddenId;
            data.project_name = vm.project_name;
            data.description = vm.description;
            //if (validation()) {

            //}
            ProjectService.save(data, function (data) {
                if (data == 1) {
                    alert('Project Added Successfully..');
                    reset();
                    fillTable();
                } else {
                    alert('Data is not Added..');
                }
            });

        };
        //function validation() {
        //    var errFalg = false;
        //    if (vm.project_name == undefined) {
        //        errFalg = true;
        //        console.log(angular.element('#p_name')[0]);
        //    }
        //}
    };

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /*                                                              Services Section                                                                          */

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    angular
        .module('MainApp.add_project')
        .service('ProjectService', ProjectService);
    ProjectService.$inject = ['$http'];

    function ProjectService($http) {
        var service = {
            fillTable: fillTable,
            getMaxId: getMaxId,
            fetchEdit: fetchEdit,
            save : save
        };
        return service;

        ///////////////////////
        function fillTable(cbs) {
            $http({
                url: 'Add_Project/fetchTable',
                method: 'POST',

            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccess
                console.log(resp);
            });
        };
        function getMaxId(cbs) {
            $http({
                url: 'Add_Project/getMaxId',
                method: 'POST',

            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccess
                console.log(resp);
            });
        }
        function fetchEdit(id, cbs, cbf) {
            $http({
                url: 'Add_Project/fetchEdit',
                method: 'POST',
                data: { 'id': id }
            }).then(function (resp) {
                //success
                if (resp.data.project_id != 0) {
                    cbs(resp.data);
                } else {
                    cbf();
                }
                
            }, function (resp) {
                //unsuccess

            });
        }
        function save(data, cbs) {
            $http({
                url: 'Add_Project/insert',
                method: 'POST',
                data: { 'd': data }
            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsucess
            });
        }
        
    }

})();
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*                                                             End Services Section                                                                   */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*                                                              Directive Section                                                                       */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

angular
    .module('MainApp.add_project')
    .directive('validationDataText', function () {
    return {
        require: 'ngModel',
        link: function ($scope, $element, $attrs, $controller) {
            //$element.bind('focus', function () {
            //    $element.removeClass('red');
            //    console.log($element[0].value);
            //});
            $element.bind('focusout', function (event) {
                event.preventDefault();
                if ($element[0].value === '') {
                    $element.addClass('red');
                } else {
                    $element.removeClass('red');
                }
            });
            $element.bind('keyup', function () {
                var regexp = /[^a-zA-Z ]/ig;
                $element[0].value = $element[0].value.replace(regexp, '');
                //console.log($element[0].value);
            });
        }
    }
});


























/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/*                                                           End Directive Section                                                                     */

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////