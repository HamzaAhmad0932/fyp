﻿/// <reference path="../angular.js" />


//Declartion of main application
(function () {
    'use strict';
    angular.module('MainApp.add_broker', []);
    angular.module('MainApp.add_broker')
    .controller('brokerController', ['$scope', 'BrokerService', '$document', '$rootScope', function ($scope, BrokerService, $document, $rootScope) {

        //$scope.validate = function () {
        //    var errorFlag = false;
        //    if ($scope.broker_id == '') {
        //        errorFlag = true;
        //        angular.element('');
        //    }
        //}
        //console.log($document);
        $scope.edit = function (id) {
            var _id = (id.broker_id == undefined) ? 0 : id.broker_id;
            if (_id != 0) {
                BrokerService.FetchEdit(id.broker_id, function (resp) {
                    $scope.hidden = resp.broker_id;
                    $scope.broker_name = resp.broker_name;
                    $scope.area_covered = resp.area_covered;
                    $scope.phone = resp.phone;

                }, function () {
                    alert('Data Not Found..');
                    getMaxId();
                    reset();

                });
            }
        };
        var validation = function () {
            var errFlag = false;
            if ($scope.broker_id == undefined) {
                errFlag = true;
            }
            if ($scope.broker_name == undefined) {
                errFlag = true;
            }
            if ($scope.area_covered == undefined || $scope.area_covered == '') {
                errFlag = true;
            }
            if ($scope.phone == undefined) {
                errFlag = true;
            }

            return errFlag;
        };
        $scope.save = function () {
            if (validation()) {
                alert('Correct The Errors .. ');
                return false;
            } else {
                var data = {};

                data.broker_id = $scope.hidden;
                data.broker_name = $scope.broker_name;
                data.area_covered = $scope.area_covered;
                data.phone = $scope.phone;
                BrokerService.save(data, function (resp) {
                    if (resp == 1) {
                        alert("Broker Saved Successfully..");
                        getMaxId();
                        reset();
                        fillTable();
                    } else {
                        alert("Error While Saving..");
                    }
                });
            }

        };
        //$scope.$watch('broker_name', function (newValue, oldValue) {
        //    if (newValue == undefined) {
        //        $scope.namechk = true;
        //        //$scope.dis = true;
        //        //console.log('new Value : ' + newValue);
        //    } else {
        //        $scope.dis = false;
        //    }

        //}, true);

        var reset = function () {
            $scope.hidden = '';
            $scope.broker_name = '';
            $scope.area_covered = '';
            $scope.phone = '';
        };

        var getMaxId = function () {
            BrokerService.getMaxId(function (resp) {
                $scope.broker_id = resp.data;
            });
        };
        var fillTable = function () {
            BrokerService.fetch(function (resp) {
                $scope.populateTable = resp.data;
            });
        };


        //Controller function call
        getMaxId();
        fillTable();
        //controller function call
    }]);
    //-------------------------------------------------------------service section------------------------------------------------------------------------//
    angular.module('MainApp.add_broker')
    .service('BrokerService', ['$http', function ($http) {
        this.getMaxId = function (showId) {
            $http({
                url: 'Add_Broker/GetMaxId',
                method: 'POST',

            }).then(function (resp) {
                showId(resp);
            }, function (resp) { });
        }
        //end getmaxid

        this.fetch = function (cb) {
            $http({
                url: 'Add_Broker/fetchAll',
                method: 'POST',
            }).then(function (resp) {
                //success
                cb(resp);
            }, function (resp) {

            });
        }
        //end fetch

        this.FetchEdit = function (_id, cbs, cbf) {
            $http({
                url: 'Add_Broker/fetchEdit',
                method: 'POST',
                data: { 'id': _id }
            }).then(function (resp) {
                //success
                //console.log(resp);
                if (resp.data.broker_id != 0) {
                    cbs(resp.data);
                } else {
                    cbf();
                }
            }, function (resp) {
                //unsuccessful

            });
        }
        //end fetchedit

        this.save = function (_data, cbs) {
            $http({
                url: 'Add_Broker/save',
                method: 'POST',
                data: { 'broker': _data },

            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccessful
                //cbf();
            });

        }

    }]);

    //-------------------------------------------------------------service section------------------------------------------------------------------------//




    //------------------------------------------------------------Directive Section--------------------------------------------------------------------//
    angular.module('MainApp.add_broker')
    .directive('validationDataText', function () {
        return {
            require: 'ngModel',
            link: function ($scope, $element, $attrs, $controller) {
                //$element.bind('focus', function () {
                //    $element.removeClass('red');
                //    console.log($element[0].value);
                //});
                $element.bind('focusout', function (event) {
                    event.preventDefault();
                    if ($element[0].value === '') {
                        $element.addClass('red');
                    } else {
                        $element.removeClass('red');
                    }
                });
                $element.bind('keyup', function () {
                    var regexp = /[^a-zA-Z ]/ig;
                    $element[0].value = $element[0].value.replace(regexp, '');
                    //console.log($element[0].value);
                });
            }


        }
    });
    angular.module('MainApp.add_broker')
    .directive('validationDataNum', function () {
        return {
            link: function ($scope, $element, $attrs, $controller) {
                $element.bind('keyup', function () {
                    var regexp = /[^0-9+]/ig;
                    $element[0].value = $element[0].value.replace(regexp, '');
                });
                $element.bind('focusout', function () {
                    if ($element[0].value == '') {
                        $element.addClass('red');
                    } else {
                        $element.removeClass('red');
                    }
                });
            }
        }

    });
    //------------------------------------------------------------------Directive Section----------------------------------------------------------------//




})();