﻿/// <reference path="../../Content/Templates/installments2.html" />
/// <reference path="../angular.js" />
(function () {
        'use strict';
        angular.module('MainApp.saleProperty', []);
        angular
            .module('MainApp.saleProperty')
            .controller('SalePropertyController', ['$scope', 'SaleService', '$interval', function ($scope, SaleService, $interval) {
                var vm = this;
                vm.fetchProperty = fetchProperty;
                vm.calculationDiff = calculationDiff;
                vm.toggle = toggle;
                vm.show1 = false;
                vm.show2 = false;
                vm.send = {};
                vm.chk = chk;
                vm.chk1 = chk1;
                vm.send.payment;
                vm.readonly = true;
                vm.imgIndic = false;
                vm.resetAll = resetAll;
                vm.popMode = [];
                vm.img;
                vm.selectFileforUpload = selectFileforUpload;
                vm.imageFile = null;
                vm.calculatePerInst = calculatePerInst;
                activate();
                //////////////////////////////////////////////
                function activate() {
                    getMaxId();
                    fillPayMode();
                    //Date updation
                    var date = new Date();
                    $interval(update, 1000);
                    function update() {
                        var date = new Date();
                        vm.send.date = date; 
                    } 
                    //Date updation
                    //image name slice
                    
                    $('#file').on('change', function (e) {
                        e.preventDefault();
                        var value = this.value;
                        vm.send.image = value.slice(12, value.length);
                        //console.log();
                    });
                    //image name slice
                };
                function fillPayMode() {
                    SaleService.fillPayModeReq(function (data) {
                        console.log(data);
                        vm.popMode = data;
                    });
                }
                function selectFileforUpload(files) {
                    //vm.imageFile 
                    var img = files[0];
                    if (!validate_image(img)) {
                        vm.imageFile = img;
                    }
                    
                }
                function validate_image(img) {
                    var errFlag = false;
                    if (img.type != 'image/jpeg' && img.type != 'image/jpg' && img.type != 'image/gif' && img.type != 'image/png') {
                        alert('Image must be JPEG, JPG, GIF, PNG Extension..');
                        errFlag = true;
                    }
                    if (img.size > (512 * 1024)) {
                        alert('Image size allow less than 512 x 1024');
                        errFlag = true;
                    }
                    return errFlag;
                }
                function toggle(d) {
                    var term = d.Sale.send.term;
                    if (term == 'netpayment') {
                        vm.show1 = true;
                        vm.show2 = false;
                    } else if (term == 'installments') {
                        vm.show1 = false;
                        vm.show2 = true;
                    } else {
                        vm.show1 = false;
                        vm.show2 = false;
                    }
                }
                function validation() {
                    var errFlag = false;
                    angular.element('#date').removeClass('red');
                    if (angular.isUndefined(vm.send.date) || vm.send.date  == '') {
                        angular.element('#date').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#property_id').removeClass('red');
                    if (angular.isUndefined(vm.send.property_id) || vm.send.property_id == '') {
                        angular.element('#property_id').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#term').removeClass('red');
                    if (angular.isUndefined(vm.send.term) || vm.send.term == '') {
                        angular.element('#term').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#advance').removeClass('red');
                    if (angular.isUndefined(vm.send.advance) || vm.send.advance == '') {
                        angular.element('#advance').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#difference').removeClass('red');
                    if (vm.send.difference != 0) {
                        angular.element('#difference').addClass('red');
                        errFlag = true;
                    }
                    //angular.element('#discount').removeClass('red');
                    //if (angular.isUndefined(vm.send.discount) || vm.send.discount == '') {
                    //    angular.element('#discount').addClass('red');
                    //    errFlag = true;
                    //}
                    //angular.element('#payment_mode').removeClass('red');
                    //if (angular.isUndefined(vm.send.payment_mode)) {
                    //    angular.element('#payment_mode').addClass('red');
                    //    errFlag = true;
                    //}
                    angular.element('#payment').removeClass('red');
                    if (angular.isUndefined(vm.send.payment) || vm.send.payment == '') {
                        angular.element('#payment').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#name').removeClass('red');
                    if (angular.isUndefined(vm.send.name) || vm.send.name == '') {
                        angular.element('#name').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#f_name').removeClass('red');
                    if (angular.isUndefined(vm.send.f_name) || vm.send.f_name == '') {
                        angular.element('#f_name').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#cnic').removeClass('red');
                    if (angular.isUndefined(vm.send.cnic) || vm.send.cnic == '') {
                        angular.element('#cnic').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#address').removeClass('red');
                    if (angular.isUndefined(vm.send.address) || vm.send.address == '') {
                        angular.element('#address').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#p_address').removeClass('red');
                    if (angular.isUndefined(vm.send.p_address) || vm.send.p_address == '') {
                        angular.element('#p_address').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#mobile').removeClass('red');
                    if (angular.isUndefined(vm.send.mobile) || vm.send.mobile == '') {
                        angular.element('#mobile').addClass('red');
                        errFlag = true;
                    }
                    vm.imgIndic = false;
                    angular.element('#image').removeClass('red');
                    if (angular.isUndefined(vm.send.image) || vm.send.image == '') {
                        angular.element('#image').addClass('red');
                        angular.element('#image').css({ 'color': 'red', 'border' : 'none' });
                        vm.imgIndic = true;
                        errFlag = true;
                    }
                    return errFlag;
                    
                }
                function validation2() {
                    var errFlag = false;
                    angular.element('#date').removeClass('red');
                    if (angular.isUndefined(vm.send.date) || vm.send.date == '') {
                        angular.element('#date').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#property_id').removeClass('red');
                    if (angular.isUndefined(vm.send.property_id) || vm.send.property_id == '') {
                        angular.element('#property_id').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#term').removeClass('red');
                    if (angular.isUndefined(vm.send.term) || vm.send.term == '') {
                        angular.element('#term').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#advance').removeClass('red');
                    if (angular.isUndefined(vm.send.advance) || vm.send.advance == '') {
                        angular.element('#advance').addClass('red');
                        errFlag = true;
                    }
                    
                    angular.element('#payment_mode').removeClass('red');
                    if (angular.isUndefined(vm.send.payment_mode) || vm.send.payment_mode == '') {
                        angular.element('#payment_mode').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#name').removeClass('red');
                    if (angular.isUndefined(vm.send.name) || vm.send.name == '') {
                        angular.element('#name').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#f_name').removeClass('red');
                    if (angular.isUndefined(vm.send.f_name) || vm.send.f_name == '') {
                        angular.element('#f_name').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#cnic').removeClass('red');
                    if (angular.isUndefined(vm.send.cnic) || vm.send.cnic == '') {
                        angular.element('#cnic').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#address').removeClass('red');
                    if (angular.isUndefined(vm.send.address) || vm.send.address == '') {
                        angular.element('#address').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#p_address').removeClass('red');
                    if (angular.isUndefined(vm.send.p_address) || vm.send.p_address == '') {
                        angular.element('#p_address').addClass('red');
                        errFlag = true;
                    }
                    angular.element('#mobile').removeClass('red');
                    if (angular.isUndefined(vm.send.mobile) || vm.send.mobile == '') {
                        angular.element('#mobile').addClass('red');
                        errFlag = true;
                    }
                    vm.imgIndic = false;
                    angular.element('#image').removeClass('red');
                    if (angular.isUndefined(vm.send.image) || vm.send.image == '') {
                        angular.element('#image').addClass('red');
                        angular.element('#image').css({ 'color': 'red', 'border': 'none' });
                        vm.imgIndic = true;
                        errFlag = true;
                    }
                    return errFlag;

                }
                function genrelValidation() {
                    angular.element('#term').removeClass('red');
                    if (angular.isUndefined(vm.send.term) || vm.send.term == '') {
                        angular.element('#term').addClass('red');
                    }
                    angular.element('#property_id').removeClass('red');
                    if (angular.isUndefined(vm.send.term) || vm.send.term == '') {

                        angular.element('#property_id').addClass('red');
                    }
                }
                //function printVoucher() {
                //    var sr = $("#sr").val();
                //    var date = $('#date').val();
                //    var plotno = $('#property_id').val();
                //    var project_name = $('#project_name').val();
                //    var sector_name = $('#sector_name').val();
                //    var street_no = $('#street_no').val();
                //    var house_no = $('#house_no').val();
                //    var width = $('#width').val();
                //    var length = $('#length').val();
                //    var marla = $('#marla').val();
                //    var covered_area = $('#covered_area').val();
                //    var rate = $('#rate').val();
                //    var cost = $('#cost').val();
                //    var total_amount = $('#total_amount').val();
                //    var term = $('#term').val();
                //    var advance = $('#advance').val();
                //    var payment_mode = $('#payment_mode').val();
                //    var per_inst = $('#per_inst').val();
                //    var payment = $('#payment').val();
                //    var name = $('#name').val();
                //    var f_name = $('#f_name').val();
                //    var cnic = $('#cnic').val();
                //    var address = $('#address').val();
                //    var p_address = $('#p_address').val();
                //    var mobile = $('#mobile').val();
                //    var ph_reserve = $('#ph_reserve').val();


                //    $("#srAssign").html(sr);

                //    $("#plotnoAssign").html(plotno);
                //    $("#project_nameAssign").html(project_name);
                //    $("#sectorAssign").html(sector_name);
                //    $("#streetAssign").html(street_no);
                //    $("#houseAssign").html(house_no);
                //    $("#widthAssign").html(width);
                //    $("#lengthAssign").html(length);
                //    $("#marlaAssign").html(marla);
                //    $("#areaAssign").html(covered_area);
                //    $("#rateAssign").html(rate);
                //    $("#costAssign").html(cost);
                //    $("#amountAssign").html(total_amount);
                //    $("#termAssign").html(term);
                //    $("#advanceAssign").html(advance);
                //    $("#modeAssign").html(payment_mode);
                //    $("#perinstAssign").html(per_inst);
                //    $("#paymentAssign").html(payment);
                //    $(".nameAssign").html(name);
                //    $("#f_nameAssign").html(f_name);
                //    $("#cnicAssign").html(cnic);
                //    $("#addressAssign").html(address);
                //    $("#p_addressAssign").html(p_address);
                //    $("#mobileAssign").html(mobile);
                //    $("#mydata").printMe({
                //        "path": ["/Content/css/example.css"],
                //        "title": "Sale/Installement Receipt"
                //    });
                //}
                function chk() {
                        if (vm.send.term == 'netpayment') {
                            if (validation()) {
                                alert('Correct the errors..');
                            } else {
                                var img = vm.imageFile;
                                var fd = new FormData();
                                fd.append('img', img);
                                //fd.append('Inst', vm.send);
                                SaleService.saveToSale(vm.send, fd, function (affect) {
                                    if (affect != -1) {
                                        //printVoucher();
                                        resetAll();
                                        alert('Data Saved Successfully..');
                                    } else {
                                        alert('Error Occured While Saving..');
                                    }
                                });
                            }
                        } else if (vm.send.term == 'installments') {
                            if (validation2()) {
                                alert('Correct the errors..');
                            } else {
                                var img = vm.imageFile;
                                var fd = new FormData();
                                fd.append('img', img);
                                //fd.append('Inst', vm.send);
                                SaleService.saveToInst(vm.send, fd, function (affect) {
                                    if (affect != -1) {
                                        //printVoucher();
                                        alert('Data Saved Successfully..');
                                        resetAll();
                                    } else {
                                        alert('Error Occured While Saving..');
                                    }
                                });
                            }
                        } else {
                            alert('Correct the errors..');
                            genrelValidation();
                        }
                }

                function resetAll() {
                    getMaxId();
                    vm.send = {};
                    vm.status = '';
                    $scope.filepreview = '';
                    $scope.file = {};
                }
                function chk1() {
                    console.log(vm.send.image);
                }
                function getMaxId() {
                    SaleService.getMaxId(function (data) {
                        if (data == -1) {
                            vm.sr = 1;
                        } else {
                            vm.sr = data;
                        }
                    });
                }
                function calculationDiff() {
                    var regex = /[^0-9\.0-9$]/gi;
                    var advance = vm.send.advance.replace(regex, '');
                    var totalAmount = vm.send.total_amount;
                    var discount = vm.send.discount;
                    var payment = vm.send.payment;
                     advance = ((angular.isUndefined(advance)) || (isNaN(advance)) || advance == '') ? parseFloat(0) : parseFloat(advance);
                     totalAmount = ((angular.isUndefined(totalAmount)) || (isNaN(totalAmount)) || totalAmount == '') ? parseFloat(0) : parseFloat(totalAmount);
                     discount = ((angular.isUndefined(discount)) || (isNaN(discount)) || discount == '') ? parseFloat(0) : parseFloat(discount);
                     payment = ((angular.isUndefined(payment)) || (isNaN(payment)) || payment == '') ? parseFloat(0) : parseFloat(payment);

                    var diff = parseFloat(totalAmount) - parseFloat(advance) - parseFloat(discount) - parseFloat(payment);
                    vm.send.difference = diff;
                }
                function fetchProperty(_id) {
                    //SaleService.fetchProperty(_id.Sale.send.plot_no, function (data) {
                    //    console.log(data);
                    //});
                    if (_id.Sale.send.property_id != undefined && _id.Sale.send.property_id != 0) {
                        SaleService.fetchProperty(_id.Sale.send.property_id, function (data) {
                            //console.log(data);
                            if (data[1].property_id != 0) {
                                //console.log(data);
                                populateProperty(data[1]);
                                vm.status = data[0];
                                toggleTerm(data[0]);
                            } else {
                                alert('Data Not Found..');
                                _id.Sale.property_id = '';
                                reset_property();
                            }
                        });
                    } else {
                        reset_property();
                    }
                }
                function toggleTerm(status) {
                    if (status == 'Available') {
                        vm.readonly = false;
                    } else {
                        vm.readonly = true;
                    }
                }
                function reset_property() {
                    getMaxId();
                    vm.send = {};
                    vm.status = '';
                    //vm.project_id = '';
                    //vm.project_name = '';
                    //vm.sector_id = '';
                    //vm.sector_name = '';
                    //vm.street_no = '';
                    //vm.house_no = '';
                    //vm.width = '';
                    //vm.length = '';
                    //vm.marla = '';
                    //vm.covered_area = '';
                    //vm.rate = '';
                    //vm.cost = '';
                    //vm.total_amount = '';
                    //vm.rate_criteria = '';
                    //vm.remarks = '';
                }
                function populateProperty(d) {
                    vm.send.project_id = d.project_id;
                    vm.send.project_name = d.project_name;
                    vm.send.sector_id = d.sector_id;
                    vm.send.sector_name = d.sector_name;
                    vm.send.street_no = d.street_no;
                    vm.send.house_no = d.house_no;
                    vm.send.width = d.width;
                    vm.send.length = d.length;
                    vm.send.marla = d.marla;
                    vm.send.covered_area = d.covered_area;
                    vm.send.rate = d.rate;
                    vm.send.total_cost = d.total_cost;
                    vm.send.total_amount = d.total_amount;
                    vm.send.rate_criteria = d.rate_criteria;
                    vm.send.category = d.category;
                    
                }
                function calculatePerInst() {
                    var diff = parseFloat(vm.send.difference);
                    var insts = parseFloat(vm.send.payment_mode);
                    var perInst = diff / insts;
                    vm.send.per_installment = perInst;
                }
                
            }]);//End Controller
    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                          Service Section                                       */
    /////////////////////////////////////////////////////////////////////////////////////////////////////
        angular
            .module('MainApp.saleProperty')
            .service('SaleService', ['$http', function ($http) {
                var service = {
                    getMaxId: getMaxId,
                    fetchProperty: fetchProperty,
                    saveToInst: saveToInst,
                    saveToSale: saveToSale,
                    fillPayModeReq: fillPayModeReq
                };
                return service;
                function getMaxId(cbs) {
                    $http({
                        url: 'Sale_Property/getMaxId',
                        method: 'POST',
                    }).then(function (resp) {
                        //success
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccess
                    });
                }
                function fetchProperty(id, cbs) {
                    $http({
                        url: 'Sale_Property/fetchProperty',
                        method: 'POST',
                        data: { 'id': id }
                    }).then(function (resp) {
                        //success
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccess
                    });
                }
                function saveToInst(Inst, fd, cbs) {
                    //file saving
                    $http.post('Sale_Property/saveToDirectory', fd, {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function (resp) {
                        //success
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccessful
                    });
                    //end file saving
                    $http({
                        url: 'Sale_Property/saveToInst',
                        method: 'POST',
                        withCredentials: true,
                        data: { 'Inst': Inst }
                        //headers: { 'Content-Type': undefined },
                        //transformRequest: angular.identity
                    }).then(function (resp) {
                        //successful
                        //cbs(resp.data);
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccessful
                    });
                    
                }
                function saveToSale(sale, fd, cbs) {
                    //file saving
                    $http.post('Sale_Property/saveToDirectory', fd, {
                        withCredentials: true,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function (resp) {
                        //success
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccessful
                    });
                    //end file saving
                    $http({
                        url: 'Sale_Property/saveToSale',
                        method: 'POST',
                        data: { 'sale': sale},
                        withCredentials: true
                        //headers: { 'Content-Type': undefined },
                        //transformRequest: angular.identity
                    }).then(function (resp) {
                        //success
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccess
                    });
                }
                function fillPayModeReq(cbs) {
                    $http({
                        url: 'Sale_Property/fillPayMode',
                        method: 'POST'
                    }).then(function (resp) {
                        //success
                        cbs(resp.data);
                    }, function (resp) {
                        //unsuccess
                    });
                }
            }]);

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                         End Service Section                                     */
    /////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////
    /*                                         Directive Section                                       */
    /////////////////////////////////////////////////////////////////////////////////////////////////////



    //////////////////////////////////////////////////////////////////////////////////////
        //angular
        //    .module('MainApp')
        //    .directive('showtoggle', function () {
        //        return {
        //            restrict: 'E',
        //            scope: {
        //                term : '@'
        //            },
        //            link: function ($scope, $element, $attrs) {
                        
        //                $scope.$watch('term', function (term) {
        //                    if (term && term.length) {
        //                        $scope.urlsrc = '/Content/Templates/' + term + '.html';
        //                    } else {
        //                        $scope.urlsrc = '';
        //                    } 
        //                });
        //            },
        //            controller: function ($scope, $element, $attrs) {
                        
        //            },
        //            template : '<ng-include src="urlsrc"></ng-include>'
        //        }
        //    });
        angular
            .module('MainApp.saleProperty')
            .directive('validDir', function () {
                var directive = {
                    link : link
                };
                return directive;
                function link($scope, $element, $attrs) {
                    $element.bind('keyup', function (e) {
                        e.preventDefault();
                        var regex = /[^0-9]/gi;
                        $element[0].value = $element[0].value.replace(regex, '');
                    });
                }
            });
        angular
                .module('MainApp.saleProperty')
                .directive('validDirAm', function () {
                    var directive = {
                        link: link
                    };
                    return directive;
                    function link($scope, $element, $attrs) {
                        $element.bind('keyup', function (e) {
                            e.preventDefault();
                            var regex = /[^0-9\.]/gi;
                            $element[0].value = $element[0].value.replace(regex, '');
                        });
                    }
                });
        angular
             .module('MainApp.saleProperty')
             .directive('validStr', function () {
                        var directive = {
                            link: link
                        };
                        return directive;
                        function link($scope, $element, $attrs) {
                            $element.bind('keyup', function (e) {
                                e.preventDefault();
                                var regex = /[^a-zA-Z ]/gi;
                                $element[0].value = $element[0].value.replace(regex, '');
                            });
                        }
                    });
        angular
            .module('MainApp.saleProperty')
            .directive('validCnic', function () {
                     var directive = {
                         link: link
                     };
                     return directive;
                     function link($scope, $element, $attrs) {
                         $element.bind('keyup', function (e) {
                             e.preventDefault();
                             var regex = /[^0-9-]/gi;
                             $element[0].value = $element[0].value.replace(regex, '');
                         });
                     }
                 });
        angular
            .module('MainApp.saleProperty')
            .directive('validAddress', function () {
                    var directive = {
                        link: link
                    };
                    return directive;
                    function link($scope, $element, $attrs) {
                        $element.bind('keyup', function (e) {
                            e.preventDefault();
                            var regex = /[^0-9a-zA-z-#\. ]/gi;
                            $element[0].value = $element[0].value.replace(regex, '');
                        });
                    }
                });
        angular
             .module('MainApp.saleProperty')
             .directive('validMob', function () {
                    var directive = {
                        link: link
                    };
                    return directive;
                    function link($scope, $element, $attrs) {
                        $element.bind('keyup', function (e) {
                            e.preventDefault();
                            var regex = /[^0-9+-]/gi;
                            $element[0].value = $element[0].value.replace(regex, '');
                        });
                    }
             });
        //angular
        //    .module('MainApp')
        //    .directive('calculate', [function () {
        //        var directive = {
        //            scope: {
        //                amount: '=',
        //                dif: '=',
        //                payment : '='
        //            },
        //            link: link,
        //            complie: compile,
        //            controller : controller
        //        };
        //        return directive;
        //        function link($scope, $element, $attrs) {
        //            $element.bind('keyup', function (keyupEvent) {
        //                var amount = parseFloat($scope.amount);
        //                var advance = parseFloat($element[0].value);
        //                var payment = parseFloat($attrs.payment);
        //                console.log(payment);
        //                //console.log("amount : "+amount+"--Advance : "+advance);
        //                $scope.dif = amount - advance;
        //                $scope.$apply();
        //            });
        //        }//end link
        //        function compile(tElement, tAttributes) {
                    
        //        }//end compile
        //        function controller($scope, $element, $attrs) {
                    
        //        }//end controller

        //    }]);

        //angular
        //    .module('MainApp')
        //    .directive('fileInput', function ($parse) {
        //        var directive = {
        //            restrict: 'A',
        //            link : link
        //        }
        //        return directive;
        //        function link($scope, $element, $attrs) {
        //            $element.bind('change', function () {
        //                $parse($attrs.fileInput.assign($scope, $element[0].files));
        //                $scope.$apply();
        //            });
        //        }
        //    });
    angular
        .module('MainApp.saleProperty')
        .directive("fileinput", [function() {
            var directive =  {
                scope: {
                    fileinput: "=",
                    filepreview: "=",
                },
                link: linkFunc
                };
                return directive;
                function linkFunc($scope, $element, $attrs) {
                    $element.bind("change", function (e) {
                        var img = e.target.files[0];
                        if (img.type == 'image/jpeg' || img.type == 'image/jpg' || img.type == 'image/gif' || img.type == 'image/png') {
                            $scope.fileinput = img;
                            //console.log($element[0].value);
                            var reader = new FileReader();
                            reader.onload = function (loadEvent) {
                                $scope.$apply(function () {
                                    $scope.filepreview = loadEvent.target.result;
                                });
                            }
                            reader.readAsDataURL($scope.fileinput);
                        }
                    });
                }
            
        }]);

    //angular
    //    .module('MainApp')
    //    .directive('paymentCalculation', [function () {
    //        var directive = {
    //            scope: {
    //                pay  : '&'
    //            },
    //            link: linkFunc,
    //            controller: dirController,
    //            controllerAs : 'vm',
    //            bindToController : true
    //        }
    //        return directive;
    //        function linkFunc($scope, $element, $attrs, controller) {
    //            $element.bind('keyup', function (e) {
    //                $scope.pay();
    //                console.log($scope);
    //                $scope.$apply();
    //            });
    //        }
    //        dirController.$inject = ['$scope', '$element', '$attrs'];
    //        function dirController($scope, $element, $attrs) {
    //            //this controller is just to pass it from link function
    //        }
            
    //    }]);
    /////////////////////////////////////////////////////////////////////////////////////////////////////
     /*                                     End Directive Section                                     */
    /////////////////////////////////////////////////////////////////////////////////////////////////////
})();



