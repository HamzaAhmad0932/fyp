﻿/// <reference path="../angular.js" />
(function () {
    'use strict';
    angular.module('MainApp.addProperty', []);

    angular
        .module('MainApp.addProperty')
        .controller('AddPropertyController', AddPropertyController);
    AddPropertyController.$inject = ['PropertyService', '$scope'];

    function AddPropertyController(PropertyService, $scope) {
        var vm = this;
        vm.amount;
        vm.reset = reset;
        vm.m = true;
        vm.s = true;
        vm.str = '';
        vm.total_amount;
        vm.save = save;
        vm.calculation = calculation;
        vm.chk = chk;
        vm.rateCal1 = rateCal1;
        vm.addCost = addCost;
        activate();
        /////////////////////////////////////
        function datepicker() {
            var date = new Date();
            vm.date = date;
        }
        function activate() {
            getMaxId();
            fillProjects();
            fillSector();
            datepicker();
        }
        function getMaxId() {
            PropertyService.getMaxId(function (data) {
                if (data == -1) {
                    vm.id = 1;
                    vm.house_no = 1;
                } else {
                    vm.id = data;
                    vm.house_no = data;
                    datepicker();
                }
            });
        }

        function chk() {
            if (this.rate_criteria == 'Per Marla') {
                vm.m = false;
                vm.str = 'marla';
                vm.rate = '';
                vm.amount = '';
                vm.total_amount = '';
            }
            if (this.rate_criteria == 'Per Square') {
                vm.m = false;
                vm.str = 'sqr';
                vm.rate = '';
                vm.amount = '';
                vm.total_amount = '';
            }
        }
        function calculation() {
            var l = (parseFloat(vm.length) == undefined) ? 0 : parseFloat(vm.length);
            var w = (parseFloat(vm.width) == undefined) ? 0 : parseFloat(vm.width);
            vm.covered_area = l * w;
            vm.marla = (parseFloat(vm.covered_area) == undefined) ? 0 : parseFloat(vm.covered_area) / 270;
            //console.log(vm.covered_area);
        }
        function validation() {
            var errFlag = false;
            angular.element('#id').removeClass('red');
            if (vm.id == undefined || vm.id == '') {
                angular.element('#id').addClass('red');
                errFlag = true;
            }
            angular.element('#date').removeClass('red');
            if (vm.date == undefined || vm.date == '') {
                angular.element('#date').addClass('red');
                errFlag = true;
            }
            angular.element('#project_id').removeClass('red');
            if (vm.project_id == undefined || vm.project_id == '') {
                angular.element('#project_id').addClass('red');
                errFlag = true;
            }
            angular.element('#sector_id').removeClass('red');
            if (vm.sector_id == undefined || vm.sector_id == '') {
                angular.element('#sector_id').addClass('red');
                errFlag = true;
            }
            angular.element('#house_no').removeClass('red');
            if (vm.house_no == undefined || vm.house_no == '') {
                angular.element('#house_no').addClass('red');
                errFlag = true;
            }
            angular.element('#street_no').removeClass('red');
            if (vm.street_no == undefined || vm.street_no == '') {
                angular.element('#street_no').addClass('red');
                errFlag = true;
            }
            angular.element('#length').removeClass('red');
            if (vm.length == undefined || vm.length == '') {
                angular.element('#length').addClass('red');
                errFlag = true;
            }
            angular.element('#width').removeClass('red');
            if (vm.width == undefined || vm.width == '') {
                angular.element('#width').addClass('red');
                errFlag = true;
            }
            angular.element('#marla').removeClass('red');
            if (vm.marla == undefined || vm.marla == '') {
                angular.element('#marla').addClass('red');
                errFlag = true;
            }
            angular.element('#covered_area').removeClass('red');
            if (vm.covered_area == undefined || vm.covered_area == '') {
                angular.element('#covered_area').addClass('red');
                errFlag = true;
            }
            angular.element('#rate_criteria').removeClass('red');
            if (vm.rate_criteria == undefined || vm.rate_criteria == '') {
                angular.element('#rate_criteria').addClass('red');
                errFlag = true;
            }
            angular.element('#rate').removeClass('red');
            if (vm.rate == undefined || vm.rate == '') {
                angular.element('#rate').addClass('red');
                errFlag = true;
            }
            angular.element('#amount').removeClass('red');
            if (vm.amount == undefined || vm.amount == '') {
                angular.element('#amount').addClass('red');
                errFlag = true;
            }
            angular.element('#total_amount').removeClass('red');
            if (vm.total_amount == undefined || vm.total_amount == '') {
                angular.element('#total_amount').addClass('red');
                errFlag = true;
            }
            angular.element('#category').removeClass('red');
            if (vm.category == undefined || vm.category == '') {
                angular.element('#category').addClass('red');
                errFlag = true;
            }
            return errFlag;  
        }
        function reset() {
            vm.hiddenid = '';
           vm.date = '';
           vm.project_id = '';
           vm.sector_id = '';
           vm.street_no = '';
           vm.length = '';
           vm.width = '';
           vm.covered_area = '';
           vm.rate_criteria = '';
           vm.rate = '';
           vm.amount = '';
           vm.total_amount = '';
           vm.category = '';
           vm.bed_room = '';
           vm.width = '';
           vm.taris = '';
           vm.bath = '';
           vm.kitchen = '';
           vm.servent_room = '';
           vm.drawing_room = '';
           vm.lounge_first_floor = '';
           vm.marla = '';
           vm.lounge_ground_floor = '';
           vm.cost = '';
           datepicker();

        }
    function save() {
        var data = {};
        data.id = vm.hiddenid;
        data.date = vm.date;
        data.project_id = vm.project_id;
        data.sector_id = vm.sector_id;
        data.house_no = vm.house_no;
        data.street_no = vm.street_no;
        data.length = vm.length;
        data.width = vm.width;
        data.marla = vm.marla;
        data.covered_area = vm.covered_area;
        data.rate_criteria = vm.rate_criteria;
        data.rate = vm.rate;
        data.amount = vm.amount;
        data.total_amount = vm.total_amount;
        data.category = vm.category;
        data.bed_room = vm.bed_room;
        data.width = vm.width;
        data.taris = vm.taris;
        data.bath = vm.bath;
        data.kitchen = vm.kitchen;
        data.servent_room = vm.servent_room;
        data.drawing_room = vm.drawing_room;
        data.lounge_first_floor = vm.lounge_first_floor;
        data.total_cost = vm.cost;
        data.lounge_ground_floor = vm.lounge_ground_floor;
        if (validation()) {
            alert('Correct the errors..');
        }
        else {
            console.log(data);
            PropertyService.save(data, function (resp) {
                if (resp == 1) {
                    alert('Data Added Successfully..');
                    reset();
                    getMaxId();
                } else {
                    alert('Error While Saving..');
                }
            });
        }
    }
    function fillProjects() {
        PropertyService.fetchProject(function (data) {
            //console.log();
            vm.projects = data;
        });
    }
    function fillSector() {
        PropertyService.fetchSector(function (data) {
            vm.sectors = data;
        });
    }
    function rateCal1() {
        var cr = 0;
        if (vm.str == 'marla') {
             cr = parseFloat(vm.marla);
        } else if(vm.str == 'sqr') {
             cr = parseFloat(vm.covered_area);
        }
        var rate = parseFloat(vm.rate);
        var amount = rate * cr;
        vm.amount = amount;
        vm.total_amount = amount;
        angular.element('#amount')[0].value = amount;
        angular.element('#total_amount')[0].value = amount;
        //console.log('total '+vm.total_amount);
    }
    function addCost() {
        PropertyService.building(function (data) {
            var bed = ((vm.bed_room == undefined) ? 0 : vm.bed_room) * data.bed;
            var l_g_f = ((vm.loung_ground_floor == undefined) ? 0 : vm.loung_ground_floor) * data.l_g_floor;
            var taris = ((vm.taris == undefined) ? 0 : vm.taris) * data.taris;
            var bath = ((vm.bath == undefined) ? 0 : vm.bath) * data.bath;
            var kitchen = ((vm.kitchen == undefined) ? 0 : vm.kitchen) * data.kitchen;
            var servent_room = ((vm.servent_room == undefined) ? 0 : vm.servent_room) * data.servent_room;
            var drawing_room = ((vm.drawing_room == undefined) ? 0 : vm.drawing_room) * data.drawing_room;
            var l_f_f = ((vm.loung_first_floor == undefined) ? 0 : vm.loung_first_floor) * data.l_first_floor;
            //Total calculated cost
            var t_cost = bed + l_g_f + taris + bath + kitchen + servent_room + drawing_room + l_f_f;
            vm.cost = t_cost;
            vm.total_amount = parseFloat((vm.amount == undefined) ? 0 : vm.amount) + parseFloat(t_cost);
            
        });
    }
    }//end controller

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////// Section Service ////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    angular
        .module('MainApp.addProperty')
        .service('PropertyService', PropertyService);
    PropertyService.$inject = ['$http'];
    function PropertyService($http) {
        var service = {
            getMaxId: getMaxId,
            save: save,
            fetchProject: fetchProject,
            fetchSector: fetchSector,
            building : building
        };
        return service;

        function getMaxId(cbs) {
            $http({
                url: 'Add_Property/getMaxId',
                method: 'POST'
            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccessful
            });
        }
        function save(data, cbs) {
            $http({
                url: 'Add_Property/save',
                method: 'POST',
                data: { 'd': data }
            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccess
            });
        }
        function fetchProject(cbs) {
            $http({
                url: 'Add_Property/fetchProject',
                method: 'POST'
            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccessful
            });
        }
        function fetchSector(cbs) {
            $http({
                url: 'Add_Property/fetchSector',
                method: 'POST'
            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccess
            });
        }
        function building(cbs) {
            $http({
                url: 'Add_Property/building',
                method: 'POST'
            }).then(function (resp) {
                //success
                cbs(resp.data);
            }, function (resp) {
                //unsuccess
            });
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////// End Section Service ////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //****************************************************************************************************************************************//
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////// Section Directive ////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    angular
        .module('MainApp.addProperty')
        .directive('validDir', valid_Dir);
    function valid_Dir() {
        var directive = {
            link : link
        };
        return directive;

        function link($scope, $element, $attrs, $controller) {
            $element.bind('focus', function (e) {
                e.preventDefault();
                $element.removeClass('red');
            });
            $element.bind('focusout', function (e) {
                //console.log($element[0].value);
                if ($element[0].value == '' || $element[0].value == -1 || $element[0].value == '? undefined:undefined ?') {
                    $element.addClass('red');
                } else {
                    $element.removeClass('red');
                }
            });
        }
    }
    angular
        .module('MainApp.addProperty')
        .directive('numDir', num_Dir);
    function num_Dir() {
        var directive = {
            link : link
        };
        return directive;
        function link($scope, $element, $attrs, $controller) {
            $element.bind('keyup', function (e) {
                var regex = /[^0-9\.]/ig;
                $element[0].value = $element[0].value.replace(regex, '');
            });
        }
    }
    angular
        .module('MainApp.addProperty')
        .directive('floatDir', float_dir);
    function float_dir() {
        var directive = {
            link : link
        };
        function link($scope, $element, $attrs, $controller) {
            $element.bind('keyup', function (e) {
                var regex = /[^0-9\.]/ig;
            });
        }
    }
    angular
        .module('MainApp.addProperty')
        .directive('rateCal', rateCal);
    function rateCal() {
        var directive = {
            scope: {
                rateFn : '&'
            },
            controller: controller
        };
        return directive;
        function controller($scope, $element, $attrs) {
            $element.bind('keyup', function (e) {
                $scope.rateFn(); 
            });   
        }
    }
    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////// End Section Directive ///////////////////////////////////////////////////////////////////

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
})();