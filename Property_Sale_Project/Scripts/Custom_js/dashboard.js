﻿/// <reference path="../angular.js" />

(function () {
    'use strict';
    angular.module('MainApp', ['ui.router',
        'angular-loading-bar',
        'ngAnimate',
        'MainApp.addProperty',
        'MainApp.saleProperty',
        'MainApp.installment',
        'MainApp.propertyview',
        'MainApp.add_project',
        'MainApp.add_broker',
        'MainApp.resaleProperty',
    ]);
    angular
        .module('MainApp')
        .config(['$stateProvider', '$locationProvider', 'cfpLoadingBarProvider', '$urlRouterProvider', function ($stateProvider, $locationProvider, cfpLoadingBarProvider, $urlRouterProvider) {
            $locationProvider.hashPrefix('!').html5Mode(true);
            cfpLoadingBarProvider.includeSpinner = false;
            
            $stateProvider
            .state('main', {
                url: '/',
                templateUrl: 'Dashboard_Main_Page/Index'
            })
            .state('addProperty', {
                url: '/Add_Property/index',
                templateUrl: 'Add_Property/index'
            })
            .state('saleProperty', {
                url: '/Sale_Property/sale_Property',
                templateUrl: 'Sale_Property/sale_Property'
            })
            .state('installment', {
                url: '/Installment_Recipt_Voucher/installment_Recipt_Voucher',
                templateUrl: 'Installment_Recipt_Voucher/installment_Recipt_Voucher'
            })
            .state('propertyview', {
                url: '/Properties_View/Index',
                templateUrl: 'Properties_View/Index'
            })
            .state('addproject', {
                url: '/Add_Project/add_Project',
                templateUrl: 'Add_Project/add_Project'
            })
            .state('addbroker', {
                url: '/Add_Broker/add_Broker',
                templateUrl: 'Add_Broker/add_Broker'
            })
            .state('resale', {
                url: '/Resale/Index',
                templateUrl: 'Resale/Index'
            })
            
            $urlRouterProvider.otherwise('/');
        }]);
    angular
        .module('MainApp')
        .controller('dashboardController', ['$scope', '$location', '$state', '$stateParams', function ($scope, $location, $state, $stateParams) {
            var vm = this;
            
            
            
        }]);
})();