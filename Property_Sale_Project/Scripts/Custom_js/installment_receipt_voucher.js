﻿/// <reference path="../angular.js" />
(function () {
    angular.module('MainApp.installment', []);
    angular
        .module('MainApp.installment')
        .controller('InstallmentReciptVoucher', ['$scope', 'InstallmentService', '$interval', function ($scope, InstallmentService, $interval) {
            var vm = this;
            vm.send = {};
            vm.propertyFetch = propertyFetch;
            vm.fetchTable = fetchTable;
            vm.send_save = send_save;
            vm.reset = reset;
            vm.InstList;
            activation();
            //
            function activation() {
                getMaxId();
                $interval(dateUpdate, 2000);//update date after 2 sec
            }
            function getMaxId() {
                InstallmentService.getMaxId(function (data) {
                    if (data == -1) {
                        vm.sr = 1;
                    } else {
                        vm.sr = data;
                    }
                });
            }
            function dateUpdate() {
                var date = new Date();
                vm.send.date = date;
                
            }
            function fetchTable(_id) {
                var id = parseInt(_id.Inst.property_id);
                if (!(angular.isUndefined(id)) && id != '' && !(isNaN(id))) {
                    InstallmentService.fetchInstDetail(id, function (data) {
                        vm.InstList = data;
                    });
                } else {
                    vm.InstList = ""; //Reset the table
                }
            }
            function propertyFetch(_id) {
                var id = parseInt(_id.Inst.send.property_id);
                if (!angular.isUndefined(id) && !angular.equals(id, '') && id != 0 && !(isNaN(id))) {
                    InstallmentService.fetchProperty(id, function (data) {
                        //success callback function
                        //populateProperty(data);
                        if (data.property_id == 0) {
                            alert('Data Not Found..');
                            vm.send.property_id = '';
                        } else {
                            populateProperty(data);
                        }
                    });

                } else {
                    reset();
                }
            }
            function populateProperty(data) {
                vm.send.category = data.category;
                vm.send.total_amount = data.total_amount;
                vm.send.project_name = data.project_name;
                vm.send.street_no = (data.street == undefined) ? data.street_no : data.street;
                vm.send.sector_name = data.sector_name;
                vm.send.length = data.length;
                vm.send.width = data.width;
                vm.send.covered_area = data.covered_area;
                vm.send.marla = data.marla;
                vm.send.rate = data.rate;
                vm.send.house_no = (data.house == undefined) ? data.house_no : data.house;
                vm.send.total_cost = data.total_cost;
                vm.send.payment_mode = data.payment_mode;
                vm.send.name = data.name;
                vm.send.f_name = data.f_name;
                vm.send.address = data.address;
                vm.send.p_address = data.p_address;
                vm.send.cnic = data.cnic;
                vm.send.mobile = data.mobile;
                vm.send.ph_reserve = data.ph_reserve;
                vm.send.ph_office = data.ph_office;
                vm.send.fax = data.fax;
                vm.send.total_installments = data.total_installments;
                vm.send.per_installment = data.per_installment;
                vm.send.advance = data.advance;
                vm.send.amount_due = (data.difference == undefined) ? data.amount_due : data.difference;
                vm.send.installment_no = (data.installment_no == undefined) ? 1 : data.installment_no;
                vm.send.image = data.image;
            }
            function validation() {
                var errorFlag = false;
                angular.element('#plotno').removeClass('red');
                if (angular.isUndefined(vm.send.property_id)) {
                    angular.element('#plotno').addClass('red');
                    errorFlag = true;
                }
                angular.element('#payment').removeClass('red');
                if (angular.isUndefined(vm.send.receiveable_payment)) {
                    angular.element('#payment').addClass('red');
                    errorFlag = true;
                }
                return errorFlag;
            }
            function send_save() {
                if (validation()) {
                    alert('Correct the errors..');
                } else {
                    //console.log(vm.send);
                    InstallmentService.saveInstPlan(vm.send, function (data) {
                        console.log(data);
                        if (data == -1) {
                            alert('Error While Saving Data');
                        } else if (data == 0) {
                            alert('Data is Not Saved');
                        } else if(data == 1) {
                            alert('Data Saved Successfully..');
                            reset();
                        }
                    });
                }
            }
            //reseting all the input fields
            function reset() {
                vm.send = {};
                getMaxId();
            }
        }]);//End controller
    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    angular
        .module('MainApp.installment')
        .service('InstallmentService', ['$http', function ($http) {
            var service = {
                getMaxId: getMaxId,
                fetchProperty: fetchProperty,
                fetchDiff: fetchDiff,
                fetchInstDetail: fetchInstDetail,
                saveInstPlan : saveInstPlan
            };
            return service;
            function getMaxId(cbs) {
                $http({
                    url: 'Installment_Recipt_Voucher/getMaxId',
                    method: 'POST'
                }).then(function (resp) {
                    //success
                    cbs(resp.data);
                }, function (resp) {
                    //unsuccess
                });
            }
            function fetchProperty(pid, cbs) {
                $http({
                    url: 'Installment_Recipt_Voucher/fetchPro',
                    method: 'POST',
                    datetype : 'JSON',
                    data: { 'pid': pid },
                    header: {'content-type' : 'application/json'}
                }).then(function (resp) {
                    //success
                    cbs(resp.data);
                }, function (resp) {
                    //unsuccess
                });
            }
            function fetchDiff(pid, cbs) {
                $http({
                    url: 'Installment_Recipt_Voucher/fetchDiff',
                    method: 'POST',
                    datatype: 'JSON',
                    data: { 'pid': pid },
                    header: {'content-type' : 'application/json'}
                }).then(function (resp) {
                    //success
                    cbs(resp.data);
                }, function (resp) {
                    //unsuccess
                });
            }
            function fetchInstDetail(pid, cbs) {
                $http({
                    url: 'Installment_Recipt_Voucher/fetchTableDetail',
                    method: 'POST',
                    data: { 'pid': pid },
                    header: { 'content-type': 'application/json' }
                }).then(function (resp) {
                    //sucess
                    cbs(resp.data);
                }, function (resp) {
                    //unsucess
                });
            }
            function saveInstPlan(plan, cbs) {
                $http({
                    url: 'Installment_Recipt_Voucher/saveInstToPlan',
                    method: 'POST',
                    data: { 'plan': plan },
                    header: { 'content-type': 'application/json' }
                }).then(function (resp) {
                    //success
                    cbs(resp.data);
                }, function (resp) {
                    //unsuccess
                });
            }
        }]);//End service
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //valid number directive
    angular
        .module('MainApp.installment')
        .directive('validNumber', [function () {
            var directive = {
                link: linkFunc
            };
            return directive;
            function linkFunc($scope, $element, $attrs) {
                $element.bind('keyup', function (e) {
                    var regex = /[^0-9\.]/gi;
                    $element[0].value = $element[0].value.replace(regex, '');
                });
            }
        }]);
    //valid number directive
    angular
        .module('MainApp.installment')
        .directive('validNumber2', [function () {
            var directive = {
                link: linkFunc
            };
            return directive;
            function linkFunc($scope, $element, $attrs) {
                $element.bind('keyup', function (e) {
                    var regex = /[^0-9]/gi;
                    $element[0].value = $element[0].value.replace(regex, '');
                });
            }
        }]);
    //valid String Directive
    angular
        .module('MainApp.installment')
        .directive('validString', [function () {
            var directive = {
                link : linkFunc
            };
            return directive;
            function linkFunc($scope, $element, $attrs) {
                $element.bind('keyup', function (e) {
                    var regex = /[^a-zA-Z ]/gi;
                    $element[0].value = $element[0].value.replace(regex, '');
                });
            }
        }]);
    //valid payment directive
    angular
        .module('MainApp.installment')
        .directive('validPayment', ['InstallmentService', function (InstallmentService) {
            var directive = {
                restrict: 'EA',
                link: linkFunc,
                controller: DirController,
                controllerAs: 'vm',
                bindToController : true
            };
            return directive;
            function linkFunc($scope, $element, $attrs, controller) {
                $element.bind('keyup', function (e) {
                    //console.log($scope);
                    //console.log(controller);
                    var regex = /[^0-9\.]/;
                    var pay = $scope.Inst.send.receiveable_payment;
                    pay = pay.replace(regex, '');
                    var payment = parseFloat(pay);
                    if (payment != 0 && payment != '' && !(isNaN(payment))) {
                        InstallmentService.fetchDiff($scope.Inst.send.property_id, function (data) {
                            var amount = parseFloat(data);
                            var due = amount - payment;
                            $scope.Inst.send.amount_due = due;
                        });
                        $scope.$apply();
                        //var amount = $scope.Inst.send.amount_due;
                        //var due = amount - payment;
                        //$scope.Inst.send.amount_due = due;
                    } else {
                        InstallmentService.fetchDiff($scope.Inst.send.property_id, function (data) {
                            $scope.Inst.send.amount_due = data;//reset the amount value from database
                        });
                    }
                });
            }
            DirController.$inject = ['$scope', '$element', '$attrs'];
            function DirController($scope, $element, $attrs) {
                //This controller is created just to pass to the link function
            }

        }]);
})();//Anynomous function for auto run

