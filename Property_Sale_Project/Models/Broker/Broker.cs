﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Broker
{
    [Table("broker")]
    public class Broker
    {
        public int broker_id { get; set; }
        public string broker_name { get; set; }
        public string area_covered { get; set; }
        public string phone { get; set; }
    }
}