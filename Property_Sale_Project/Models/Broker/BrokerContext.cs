﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Broker
{
    public class BrokerContext : DbContext
    {
        public DbSet<Broker> Brokers { get; set; }
    }
}