﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Project
{
    [Table("projects")]
    public class Project
    {
        public int project_id { get; set; }
        public string project_name { get; set; }
        public string description { get; set; }
    }
}