﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Project
{
    public class ProjectContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
    }
}