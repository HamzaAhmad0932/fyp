﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
namespace Property_Sale_Project.Models.InstallmentRecipt
{
    public class InstallmentContext : DbContext
    {
        public DbSet<Installment> Installments { get; set; }
    }
}