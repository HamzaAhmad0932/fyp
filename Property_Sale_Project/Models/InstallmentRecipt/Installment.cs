﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.InstallmentRecipt
{
    public class Installment
    {
        public int sr { get; set; }
        public DateTime date { get; set; } 
        public int property_id { get; set; }
        public string term { get; set; }
        public string project_id { get; set; }
        public string project_name { get; set; }
        public string sector_id { get; set; }
        public string sector_name { get; set; }
        public int street_no { get; set; }
        public int house_no { get; set; }
        public double length { get; set; }
        public double width { get; set; }
        public double marla { get; set; }
        public double covered_area { get; set; }
        public double rate { get; set; }
        public string rate_criteria { get; set; }
        public double total_amount { get; set; }
        public double total_cost { get; set; }
        public double advance { get; set; }
        public double difference { get; set; }
        public string payment_mode { get; set; }
        public string name { get; set; }
        public string f_name { get; set; }
        public string cnic { get; set; }
        public string address { get; set; }
        public string p_address { get; set; }
        public string mobile { get; set; }
        public string ph_reserve { get; set; }
        public string ph_office { get; set; }
        public string fax { get; set; }
        public string image { get; set; }
        public double per_installment { get; set; }
        public string category { get; set; }
        public double total_installments { get; set; }
    }
}