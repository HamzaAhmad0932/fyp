﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Sector
{
    public class Sector
    {
        public int sector_id { get; set; }
        public string sector_name { get; set; }
    }
}