﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Sector
{
    public class SectorContext : DbContext
    {
        public DbSet<Sector> Sectors { get; set; }
    }
}