﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.User
{
    public class MyUser
    {
        public int id { get; set; }
        public string name { get; set; }
        public string email { get; set; }
        public string password { get; set; }
        public string status { get; set; }
    }
}