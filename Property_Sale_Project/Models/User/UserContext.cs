﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.User
{
    public class UserContext
    {
        public class SectorContext : DbContext
        {
            public DbSet<MyUser> Sectors { get; set; }
        }
    }
}