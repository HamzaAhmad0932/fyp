﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Mode
{
    public class Mode
    {
        public int mode_id { get; set; }
        public string payment_mode { get; set; }
        public double total_installments { get; set; }
    }
}