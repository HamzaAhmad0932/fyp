﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.AddProperty
{
    public class PropertyContext : DbContext
    {
        public DbSet<Property> Properties { get; set; }
        
    }
}