﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Sale_Property
{
    public class Property
    {
        public int property_id { get; set; }
        public int project_id { get; set; }
        public string project_name { get; set; }
        public int sector_id { get; set; }
        public string sector_name { get; set; }
        public int house_no { get; set; }
        public int street_no { get; set; }
        public double marla { get; set; }
        public double covered_area { get; set; }
        public double rate { get; set; }
        public double amount { get; set; }
        public string category { get; set; }
        public double total_amount { get; set; }
        public double total_cost { get; set; }
        //public double cost { get; set; }
        public string rate_criteria { get; set; }
        public DateTime date { get; set; }
        public double length { get; set; }
        public double width { get; set; }
        public string status { get; set; }
    }
}