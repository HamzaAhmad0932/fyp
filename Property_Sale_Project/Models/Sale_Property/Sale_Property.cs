﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Sale_Property
{
    public class Sale_Property
    {
        public int sr { get; set; }
        public DateTime date { get; set; }
        public int property_id { get; set; }
        public string term { get; set; }
        public int project_id { get; set; }
        public int sector_id { get; set; }
        public int street_no { get; set; }
        public int house_no { get; set; }
        public float length { get; set; }
        public float width { get; set; }
        public float marla { get; set; }
        public float covered_area { get; set; }
        public float rate { get; set; }
        public float total_amount { get; set; }
        public float total_cost { get; set; }
        public float advance { get; set; }
        public float difference { get; set; }
        public float payment { get; set; }
        public string name { get; set; }
        public string f_name { get; set; }
        public string cnic { get; set; }
        public string address { get; set; }
        public string p_address { get; set; }
        public string mobile { get; set; }
        public string ph_reserve { get; set; }
        public string ph_office { get; set; }
        public string fax { get; set; }
        public string broker_involve { get; set; }
        public string broker_name { get; set; }
        public float broker_percent { get; set; }
        public float commission { get; set; }
        public string image { get; set; }
        public string rate_criteria { get; set; }
        public float discount { get; set; }
        public string category { get; set; }

    }
}