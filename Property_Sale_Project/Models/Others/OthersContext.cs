﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Others
{
    public class OthersContext : DbContext
    {
        public DbSet<Other> Others { get; set; }
    }
}