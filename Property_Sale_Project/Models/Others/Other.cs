﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.Models.Others
{
    public class Other
    {
        public float bed { get; set; }
        public float l_g_floor { get; set; }
        public float taris { get; set; }
        public float bath { get; set; }
        public float kitchen { get; set; }
        public float servent_room { get; set; }
        public float drawing_room { get; set; }
        public float l_first_floor { get; set; }
        public float inst_incre { get; set; }
    }
}