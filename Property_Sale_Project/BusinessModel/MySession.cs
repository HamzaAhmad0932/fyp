﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace Property_Sale_Project.BusinessModel
{
    public class MySession:IReadOnlySessionState
    {
        private MySession()
        {
            USerID = 0;
            UserName = "";
        }

        public static MySession Current
        {
            get 
            {
                MySession  session = (MySession)HttpContext.Current.Session["__MySession__"];
                if (session==null)
                {
                    session = new MySession();
                    HttpContext.Current.Session["__MySession__"] = session;
                }
                return session;
            }
        }


        public int USerID { get; set; }

        public string UserName { get; set; }
    }
}