﻿using Property_Sale_Project.Models.AddProperty;
using Property_Sale_Project.Models.Others;
using Property_Sale_Project.Models.Project;
using Property_Sale_Project.Models.Sector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.BusinessModel.Add_PropertyModel
{
    public class PropertiesBusinessModel
    {
        private string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;

        public int getMaxId()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                int id = -1;
                SqlCommand cmd = new SqlCommand("Select max(property_id)+1 as property_id from Properties", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                     if(reader["property_id"].ToString() != ""){
                         id = Convert.ToInt32(reader["property_id"]);
                     }
                    
                }
                con.Close();
                return id;

            }

        }
        public List<Property> getAllProperties()
        {
            List<Property> propertylist = new List<Property>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select p.property_id, p.category, p.length, p.width, p.marla, p.covered_area, p.rate, p.amount, p.total_cost,p.total_amount, p.status, pr.project_name, sec.sector_name from Properties p join Projects pr on(p.project_id = pr.project_id) join sector sec on(p.sector_id = sec.sector_id)", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Property property = new Property();
                    property.property_id = Convert.ToInt32(reader["property_id"]);
                    property.project_name = reader["project_name"].ToString();
                    property.sector_name = reader["sector_name"].ToString();
                    property.category = reader["category"].ToString();
                    property.length = Convert.ToDouble(reader["length"]);
                    property.width = Convert.ToDouble(reader["width"]);
                    property.marla = Convert.ToDouble(reader["marla"]);
                    property.covered_area = Convert.ToDouble(reader["covered_area"]);
                    property.rate = Convert.ToDouble(reader["rate"]);
                    property.amount = Convert.ToDouble(reader["amount"]);
                    property.total_cost = Convert.ToDouble(reader["total_cost"]);
                    property.total_amount = Convert.ToDouble(reader["total_amount"]);
                    property.status = reader["status"].ToString();
                    propertylist.Add(property);
                }
                return propertylist;
            }

        }
        public int insert(Property d)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("insert_property", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@project_id", d.project_id));
                cmd.Parameters.Add(new SqlParameter("@sector_id", d.sector_id));
                cmd.Parameters.Add(new SqlParameter("@house_no", d.house_no));
                cmd.Parameters.Add(new SqlParameter("@street_no", d.street_no));
                cmd.Parameters.Add(new SqlParameter("@marla", d.marla));
                cmd.Parameters.Add(new SqlParameter("@covered_area", d.covered_area));
                cmd.Parameters.Add(new SqlParameter("@rate", d.rate));
                cmd.Parameters.Add(new SqlParameter("@amount", d.amount));
                cmd.Parameters.Add(new SqlParameter("@category", d.category));
                cmd.Parameters.Add(new SqlParameter("@total_amount", d.total_amount));
                cmd.Parameters.Add(new SqlParameter("@total_cost", d.total_cost));
                cmd.Parameters.Add(new SqlParameter("@bed_room", d.bed_room));
                cmd.Parameters.Add(new SqlParameter("@lounge_ground_floor", d.lounge_ground_floor));
                cmd.Parameters.Add(new SqlParameter("@taris", d.taris));
                cmd.Parameters.Add(new SqlParameter("@bath", d.bath));
                cmd.Parameters.Add(new SqlParameter("@kitchen", d.kitchen));
                cmd.Parameters.Add(new SqlParameter("@servent_room", d.servent_room));
                cmd.Parameters.Add(new SqlParameter("@drawing_room", d.drawing_room));
                cmd.Parameters.Add(new SqlParameter("@lounge_first_floor", d.lounge_first_floor));
                cmd.Parameters.Add(new SqlParameter("@rate_criteria", d.rate_criteria));
                cmd.Parameters.Add(new SqlParameter("@date", d.date));
                cmd.Parameters.Add(new SqlParameter("@length", d.length));
                cmd.Parameters.Add(new SqlParameter("@width", d.width));
                con.Open();
                int affect = cmd.ExecuteNonQuery();
                con.Close();
                return affect;
            }
        }
        public List<Project> fetchProject()
        {
            List<Project> projects = new List<Project>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select * from Projects", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Project project = new Project();
                    project.project_id = Convert.ToInt32(reader["project_id"]);
                    project.project_name = reader["project_name"].ToString();
                    projects.Add(project);
                }
                return projects;
            }
        }
        public List<Sector> fetchSector()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                List<Sector> sectors = new List<Sector>();
                SqlCommand cmd = new SqlCommand("Select * from sector", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Sector sector = new Sector();
                    sector.sector_id = Convert.ToInt32(reader["sector_id"]);
                    sector.sector_name = reader["sector_name"].ToString();
                    sectors.Add(sector);
                }
                return sectors;
            }
        }
        public Other building()
        {
            
            using (SqlConnection con = new SqlConnection(constr))
            {
                Other other = new Other();
                SqlCommand cmd = new SqlCommand("Select * from others", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    
                    other.bed = Convert.ToInt32(reader["bed"]);
                    other.l_g_floor = Convert.ToInt32(reader["l_g_floor"]);
                    other.taris = Convert.ToInt32(reader["taris"]);
                    other.bath = Convert.ToInt32(reader["bath"]);
                    other.kitchen = Convert.ToInt32(reader["kitchen"]);
                    other.servent_room = Convert.ToInt32(reader["servent_room"]);
                    other.drawing_room = Convert.ToInt32(reader["drawing_room"]);
                    other.l_first_floor = Convert.ToInt32(reader["l_first_floor"]);
                    other.inst_incre = Convert.ToInt32(reader["inst_incre"]);
                }
                con.Close();
                return other;
            }
        }
    }
}