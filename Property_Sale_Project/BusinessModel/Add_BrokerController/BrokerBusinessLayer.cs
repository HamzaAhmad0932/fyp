﻿using Property_Sale_Project.Models.Broker;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.BusinessModel.Add_BrokerController
{
    public class BrokerBusinessLayer
    {
        //Declartion of connetion string that will be used in each business layer method
        private string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;
        //public BrokerBusinessLayer()
        //{
        //    //constructor
        //}
        public int getMaxId()
        {
            int id = 0;
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("select max(broker_id)+1 as broker_id from broker", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (reader["broker_id"].ToString() != "")
                    {
                        id = Convert.ToInt32(reader["broker_id"]);
                    }

                }
                con.Close();
                return id;
            }
        }
        public List<Broker> fetchAll()
        {
            List<Broker> brokers = new List<Broker>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select * from broker", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Broker broker = new Broker();
                    broker.broker_id = Convert.ToInt32(reader["broker_id"]);
                    broker.broker_name = reader["broker_name"].ToString();
                    broker.area_covered = reader["area_covered"].ToString();
                    broker.phone = reader["phone"].ToString();
                    //adding single broker to broker list
                    brokers.Add(broker);
                }
                con.Close();

            }
            return brokers;
        }

        public Broker fetchedit(int id)
        {
            Broker broker = new Broker();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select * from broker where broker_id ="+id+"", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    broker.broker_id = Convert.ToInt32(reader["broker_id"]);
                    broker.broker_name = reader["broker_name"].ToString();
                    broker.area_covered = reader["area_covered"].ToString();
                    broker.phone = reader["phone"].ToString();
                }
                con.Close();
                return broker;
            }
        }

        public int save_broker(Broker broker)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                if (broker.broker_id.Equals(0))
                {
                    SqlCommand cmd = new SqlCommand("insert_broker", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@b_name", broker.broker_name));
                    cmd.Parameters.Add(new SqlParameter("@b_area_covered", broker.area_covered));
                    cmd.Parameters.Add(new SqlParameter("@b_phone", broker.phone));
                    con.Open();
                    int affect = cmd.ExecuteNonQuery();
                    con.Close();
                    return affect;
                }
                else
                {
                    SqlCommand cmd = new SqlCommand("update_broker", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@b_id", broker.broker_id));
                    cmd.Parameters.Add(new SqlParameter("@b_name", broker.broker_name));
                    cmd.Parameters.Add(new SqlParameter("@b_area_covered", broker.area_covered));
                    cmd.Parameters.Add(new SqlParameter("@b_phone", broker.phone));
                    con.Open();
                    int affect = cmd.ExecuteNonQuery();
                    con.Close();
                    return affect;

                }
                
            }
        }


    }//class end
}//namespace end