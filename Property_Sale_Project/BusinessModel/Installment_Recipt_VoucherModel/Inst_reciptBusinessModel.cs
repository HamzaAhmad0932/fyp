﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Property_Sale_Project.Models.InstallmentRecipt;

namespace Property_Sale_Project.BusinessModel.Installment_Recipt_VoucherModel
{
    public class Inst_reciptBusinessModel
    {
        private static string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;

        public int getMaxId()
        {
            int id = -1;
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select max(vr#)+1 as vr from installment_plan", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (!(reader["vr"] is DBNull))
                    {
                        id = Convert.ToInt32(reader["vr"]);
                    }
                }
                return id;
            }
        }
        public Installment_plan fetchPropertyInstPlan(int pid)
        {
            Installment_plan inst = new Installment_plan();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("fetch_inst_plan", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id", pid));
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    inst.vr = Convert.ToInt32(reader["vr#"]);
                    inst.property_id = Convert.ToInt32(reader["property_id"]);
                    inst.project_name = reader["project_name"].ToString();
                    inst.category = reader["category"].ToString();
                    inst.sector_name = reader["sector_name"].ToString();
                    inst.street_no = Convert.ToInt32(reader["street_no"]);
                    inst.house_no = Convert.ToInt32(reader["house_no"]);
                    inst.covered_area = Convert.ToInt32(reader["covered_area"]);
                    inst.rate = Convert.ToDouble(reader["rate"]);
                    inst.total_cost = Convert.ToDouble(reader["total_cost"]);
                    inst.total_amount = Convert.ToDouble(reader["total_amount"]);
                    inst.payment_mode = reader["payment_mode"].ToString();
                    inst.total_installments = Convert.ToInt32(reader["total_installments"]);
                    inst.advance = Convert.ToDouble(reader["advance"]);
                    inst.amount_due = Convert.ToDouble(reader["amount_due"]);
                    inst.installment_no = Convert.ToInt32(reader["installment_no"]);
                    inst.per_installment = Convert.ToDouble(reader["per_installment"]);
                    inst.name = reader["name"].ToString();
                    inst.f_name = reader["f_name"].ToString();
                    inst.cnic = reader["cnic"].ToString();
                    inst.address = reader["address"].ToString();
                    inst.p_address = reader["p_address"].ToString();
                    inst.mobile = reader["mobile"].ToString();
                    inst.ph_office = reader["ph_office"].ToString();
                    inst.ph_reserve = reader["ph_reserve"].ToString();
                    inst.fax = reader["fax"].ToString();
                    inst.length = Convert.ToInt32(reader["length"]);
                    inst.width = Convert.ToInt32(reader["width"]);
                    inst.marla = Convert.ToDouble(reader["marla"]);
                }
                con.Close();
            }
            return inst;
        }
        public double diffInst(int id)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                double diff = 0;
                SqlCommand cmd = new SqlCommand("Select difference from installments where property_id = " + id + "", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                     diff = Convert.ToDouble(reader["difference"]);
                }
                con.Close();
                return diff;
            }
        }
        public double diffInstPlan(int id)
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                double due = 0;
                SqlCommand cmd = new SqlCommand("Select min(amount_due) as amount_due from installment_plan where property_id = " + id + "", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if(!(reader["amount_due"] is DBNull))
                    due = Convert.ToDouble(reader["amount_due"]);
                }
                con.Close();
                return due;
            }
        }
        public List<Installment_plan> fetchInstPlanDetail(int pid)
        {
            List<Installment_plan> inst_plans = new List<Installment_plan>();
            using (SqlConnection con = new SqlConnection(constr))
            {

                SqlCommand cmd = new SqlCommand("Select * from installment_plan where property_id = "+pid+"", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Installment_plan inst = new Installment_plan();
                    inst.vr = Convert.ToInt32(reader["vr#"]);
                    inst.date = reader["date"].ToString();
                    inst.property_id = Convert.ToInt32(reader["property_id"]);
                    inst.address = reader["address"].ToString();
                    inst.p_address = reader["p_address"].ToString();
                    inst.advance = Convert.ToInt32(reader["advance"]);
                    inst.category = reader["category"].ToString();
                    inst.cnic = reader["cnic"].ToString();
                    inst.covered_area = Convert.ToDouble(reader["covered_area"]);
                    inst.amount_due = Convert.ToDouble(reader["amount_due"]);
                    inst.f_name = reader["f_name"].ToString();
                    inst.fax = reader["fax"].ToString();
                    inst.house_no = Convert.ToInt32(reader["house_no"]);
                    //inst.image = reader["image"].ToString();
                    inst.length = Convert.ToDouble(reader["length"]);
                    inst.marla = Convert.ToDouble(reader["marla"]);
                    inst.mobile = reader["mobile"].ToString();
                    inst.name = reader["name"].ToString();
                    inst.payment_mode = reader["payment_mode"].ToString();
                    inst.per_installment = Convert.ToDouble(reader["per_installment"]);
                    inst.ph_office = reader["ph_office"].ToString();
                    inst.ph_reserve = reader["ph_reserve"].ToString();
                    inst.project_name = reader["project_name"].ToString();
                    inst.rate = Convert.ToDouble(reader["rate"]);
                    inst.sector_name = reader["sector_name"].ToString();
                    inst.street_no = Convert.ToInt32(reader["street_no"]);
                    inst.remarks = reader["remarks"].ToString();
                    inst.total_amount = Convert.ToDouble(reader["total_amount"]);
                    inst.total_cost = Convert.ToDouble(reader["total_cost"]);
                    inst.total_installments = Convert.ToInt32(reader["total_installments"]);
                    inst.width = Convert.ToDouble(reader["width"]);
                    inst.receiveable_payment = Convert.ToDouble(reader["receiveable_payment"]);
                    inst.installment_no = Convert.ToInt32(reader["installment_no"]);
                    
                    inst_plans.Add(inst);
                }
                return inst_plans;
            }
        }
        public int insertToInstPlan(Installment_plan data)
        {
            int affect = -1;
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("insert_inst_plan", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@date", data.date));
                cmd.Parameters.Add(new SqlParameter("@property_id", data.property_id));
                cmd.Parameters.Add(new SqlParameter("@project_name", data.project_name));
                cmd.Parameters.Add(new SqlParameter("@category", data.category));
                cmd.Parameters.Add(new SqlParameter("@sector_name", data.sector_name));
                cmd.Parameters.Add(new SqlParameter("@street_no", data.street_no));
                cmd.Parameters.Add(new SqlParameter("@house_no", data.house_no));
                cmd.Parameters.Add(new SqlParameter("@covered_area", data.covered_area));
                cmd.Parameters.Add(new SqlParameter("@rate", data.rate));
                cmd.Parameters.Add(new SqlParameter("@total_cost", data.total_cost));
                cmd.Parameters.Add(new SqlParameter("@total_amount", data.total_amount));
                cmd.Parameters.Add(new SqlParameter("@payment_mode", data.payment_mode));
                cmd.Parameters.Add(new SqlParameter("@total_installments", data.total_installments));
                cmd.Parameters.Add(new SqlParameter("@advance", data.advance));
                cmd.Parameters.Add(new SqlParameter("@amount_due", data.amount_due));
                cmd.Parameters.Add(new SqlParameter("@installment_no", data.installment_no));
                cmd.Parameters.Add(new SqlParameter("@receiveable_payment", data.receiveable_payment));
                cmd.Parameters.Add(new SqlParameter("@per_installment", data.per_installment));
                cmd.Parameters.Add(new SqlParameter("@remarks", (data.remarks == null) ? "" : data.remarks));
                cmd.Parameters.Add(new SqlParameter("@name", data.name));
                cmd.Parameters.Add(new SqlParameter("@f_name", data.f_name));
                cmd.Parameters.Add(new SqlParameter("@cnic", data.cnic));
                cmd.Parameters.Add(new SqlParameter("@address", data.address));
                cmd.Parameters.Add(new SqlParameter("@mobile", data.mobile));
                cmd.Parameters.Add(new SqlParameter("@ph_reserve", (data.ph_reserve == null) ? "" : data.ph_reserve));
                cmd.Parameters.Add(new SqlParameter("@ph_office", (data.ph_office == null) ? "" : data.ph_office));
                cmd.Parameters.Add(new SqlParameter("@fax", (data.fax == null) ? "" : data.fax));
                cmd.Parameters.Add(new SqlParameter("@length", data.length));
                cmd.Parameters.Add(new SqlParameter("@width", data.width));
                cmd.Parameters.Add(new SqlParameter("@marla", data.marla));
                cmd.Parameters.Add(new SqlParameter("@p_address", data.p_address));
                con.Open();
                affect =  cmd.ExecuteNonQuery();
                con.Close();

            }
            return affect;
        }
        public Installment fetchPropertyInst(int pid)
        {
            Installment inst = new Installment();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("fetchPropertyFromInst", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id", pid));
                //SqlCommand cmd = new SqlCommand("Select * from installments where property_id = " + pid + "", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    inst.sr = Convert.ToInt32(reader["sr#"]);
                    inst.property_id = Convert.ToInt32(reader["property_id"]);
                    inst.address = reader["address"].ToString();
                    inst.p_address = reader["p_address"].ToString();
                    inst.advance = Convert.ToInt32(reader["advance"]);
                    inst.project_name = reader["project_name"].ToString();
                    inst.category = reader["category"].ToString();
                    inst.cnic = reader["cnic"].ToString();
                    inst.covered_area = Convert.ToDouble(reader["covered_area"]);
                    inst.difference = Convert.ToDouble(reader["difference"]);
                    inst.f_name = reader["f_name"].ToString();
                    inst.fax = reader["fax"].ToString();
                    inst.house_no = Convert.ToInt32(reader["house_no"]);
                    inst.image = reader["image"].ToString();
                    inst.length = Convert.ToDouble(reader["length"]);
                    inst.marla = Convert.ToDouble(reader["marla"]);
                    inst.mobile = reader["mobile"].ToString();
                    inst.name = reader["name"].ToString();
                    inst.payment_mode = reader["payment_mode"].ToString();
                    inst.per_installment = Convert.ToDouble(reader["per_installment"]);
                    inst.ph_office = reader["ph_office"].ToString();
                    inst.ph_reserve = reader["ph_reserve"].ToString();
                    inst.project_id = reader["project_id"].ToString();
                    inst.rate = Convert.ToDouble(reader["rate"]);
                    inst.sector_name = reader["sector_name"].ToString();
                    inst.street_no = Convert.ToInt32(reader["street_no"]);
                    inst.term = reader["term"].ToString();
                    inst.total_amount = Convert.ToDouble(reader["total_amount"]);
                    inst.total_cost = Convert.ToDouble(reader["total_cost"]);
                    inst.total_installments = Convert.ToInt32(reader["total_installments"]);
                    inst.width = Convert.ToDouble(reader["width"]);
                }
                con.Close();
            }
            return inst;
        }
    }
}