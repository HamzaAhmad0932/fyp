﻿
using Property_Sale_Project.Models.Sale_Property;
using Property_Sale_Project.Models.InstallmentRecipt;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using Property_Sale_Project.Models.Mode;

namespace Property_Sale_Project.BusinessModel.ResaleModel
{
    public class ResalePropertyBusinessModel
    {
        private static string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;

        public int getMaxId()
        {
            int id = -1;
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select max(sr#)+1 as sr from sale_property", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    if (!(reader["sr"] is DBNull))
                    {
                        id = Convert.ToInt32(reader["sr"]);
                    }
                }
                return id;
            }
        }
        public List<Mode> fetchPayMode()
        {
            List<Mode> mode = new List<Mode>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select * from mode", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Mode m = new Mode();
                    m.mode_id = Convert.ToInt32(reader["mode_id"]);
                    m.payment_mode = reader["payment_mode"].ToString();
                    m.total_installments = Convert.ToDouble(reader["total_installments"]);
                    mode.Add(m);
                }
                con.Close();
                return mode;
            }
        }
        public Property fetchPro(int _id)
        {
            Property property = new Property();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("PropertyDetailFetchInSaleProperty", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@p_id", _id));
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    property.property_id = Convert.ToInt32(reader["property_id"]);
                    property.project_id = Convert.ToInt32(reader["project_id"]);
                    property.project_name = reader["project_name"].ToString();
                    property.sector_id = Convert.ToInt32(reader["sector_id"]);
                    property.sector_name = reader["sector_name"].ToString();
                    property.house_no = Convert.ToInt32(reader["house_no"]);
                    property.street_no = Convert.ToInt32(reader["street_no"]);
                    property.length = Convert.ToDouble(reader["length"]);
                    property.width = Convert.ToDouble(reader["width"]);
                    property.marla = Convert.ToDouble(reader["marla"]);
                    property.covered_area =  Convert.ToDouble(reader["covered_area"]);
                    property.rate = Convert.ToDouble(reader["rate"]);
                    property.rate_criteria = reader["rate_criteria"].ToString();
                    property.total_cost = Convert.ToDouble(reader["total_cost"]);
                    property.amount = Convert.ToDouble(reader["amount"]);
                    property.category = reader["category"].ToString();
                    property.total_amount = Convert.ToDouble(reader["total_amount"]);
                }
                return property;
            }
        }
        public Installment_plan fetchInst(int _id)
        {
            Installment_plan property = new Installment_plan();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("SELECT top 1 * FROM installment_plan where property_id = " + _id + " ORDER BY property_id DESC", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    //property.property_id = Convert.ToInt32(reader["installment_no"]);
                    //property.property_id = Convert.ToInt32(reader["receiveable_payment"]);

                    property.property_id = Convert.ToInt32(reader["property_id"]);
                    //property.project_id = Convert.ToInt32(reader["project_id"]);
                    property.project_name = reader["project_name"].ToString();
                    property.sector_name = reader["sector_name"].ToString();
                    //property.sector_name = reader["sector_name"].ToString();
                    property.house_no = Convert.ToInt32(reader["house_no"]);
                    property.street_no = Convert.ToInt32(reader["street_no"]);
                    property.length = Convert.ToDouble(reader["length"]);
                    property.width = Convert.ToDouble(reader["width"]);
                    property.marla = Convert.ToDouble(reader["marla"]);
                    property.covered_area = Convert.ToDouble(reader["covered_area"]);
                    property.rate = Convert.ToDouble(reader["rate"]);
                    //property.rate_criteria = reader["rate_criteria"].ToString();
                    property.total_cost = Convert.ToDouble(reader["total_cost"]);
                    //property.amount = Convert.ToDouble(reader["total_amount"]);
                    property.category = reader["category"].ToString();
                    property.total_amount = Convert.ToDouble(reader["total_amount"]);
                }
                return property;
            }
        }
        public Property fetchSale(int _id)
        {
            Property property = new Property();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("SaleDetailFetchToSaleProperty", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@id", _id));
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    property.property_id = Convert.ToInt32(reader["property_id"]);
                    //property.project_id = Convert.ToInt32(reader["project_id"]);
                    property.project_name = reader["project_name"].ToString();
                    //property.sector_id = Convert.ToInt32(reader["sector_id"]);
                    property.sector_name = reader["sector_name"].ToString();
                    property.house_no = Convert.ToInt32(reader["house_no"]);
                    property.street_no = Convert.ToInt32(reader["street_no"]);
                    property.length = Convert.ToDouble(reader["length"]);
                    property.width = Convert.ToDouble(reader["width"]);
                    property.marla = Convert.ToDouble(reader["marla"]);
                    property.covered_area = Convert.ToDouble(reader["covered_area"]);
                    property.rate = Convert.ToDouble(reader["rate"]);
                    property.rate_criteria = reader["rate_criteria"].ToString();
                    property.total_cost = Convert.ToDouble(reader["total_cost"]);
                    property.amount = Convert.ToDouble(reader["total_amount"]);
                    property.category = reader["category"].ToString();
                    property.total_amount = Convert.ToDouble(reader["total_amount"]);
                }
                return property;
            }
        }
        public int saveToSale(Sale_Property sale)
        {
            int affect = 0;
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("SaleTableInsert", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@date", sale.date));
                cmd.Parameters.Add(new SqlParameter("@property_id", sale.property_id));
                cmd.Parameters.Add(new SqlParameter("@term", sale.term));
                cmd.Parameters.Add(new SqlParameter("@project_id", sale.project_id));
                cmd.Parameters.Add(new SqlParameter("@sector_id", sale.sector_id));
                cmd.Parameters.Add(new SqlParameter("@street_no", sale.street_no));
                cmd.Parameters.Add(new SqlParameter("@house_no", sale.house_no));
                cmd.Parameters.Add(new SqlParameter("@length", sale.length));
                cmd.Parameters.Add(new SqlParameter("@width", sale.width));
                cmd.Parameters.Add(new SqlParameter("@marla", sale.marla));
                cmd.Parameters.Add(new SqlParameter("@covered_area", sale.covered_area));
                cmd.Parameters.Add(new SqlParameter("@rate", sale.rate));
                cmd.Parameters.Add(new SqlParameter("@total_amount", sale.total_amount));
                cmd.Parameters.Add(new SqlParameter("@total_cost", sale.total_cost));
                cmd.Parameters.Add(new SqlParameter("@advance", sale.advance));
                cmd.Parameters.Add(new SqlParameter("@difference", sale.difference));
                cmd.Parameters.Add(new SqlParameter("@payment", sale.payment));
                cmd.Parameters.Add(new SqlParameter("@name", sale.name));
                cmd.Parameters.Add(new SqlParameter("@f_name", sale.f_name));
                cmd.Parameters.Add(new SqlParameter("@cnic", sale.cnic));
                cmd.Parameters.Add(new SqlParameter("@address", sale.address));
                cmd.Parameters.Add(new SqlParameter("@p_address", sale.p_address));
                cmd.Parameters.Add(new SqlParameter("@mobile", sale.mobile));
                cmd.Parameters.Add(new SqlParameter("@ph_reserve", (sale.ph_reserve == null)? "" : sale.ph_reserve));
                cmd.Parameters.Add(new SqlParameter("@ph_office", (sale.ph_office == null)? "" : sale.ph_office));
                cmd.Parameters.Add(new SqlParameter("@fax", (sale.fax == null)? "" : sale.fax));
                cmd.Parameters.Add(new SqlParameter("@broker_involved", (sale.broker_involve == null) ? "" : sale.broker_involve));
                cmd.Parameters.Add(new SqlParameter("@broker_name", (sale.broker_name == null) ? "" : sale.broker_name));
                cmd.Parameters.Add(new SqlParameter("@broker_percent", sale.broker_percent));
                cmd.Parameters.Add(new SqlParameter("@commision", sale.commission));
                cmd.Parameters.Add(new SqlParameter("@image", sale.image));
                cmd.Parameters.Add(new SqlParameter("@rate_criteria", sale.rate_criteria));
                cmd.Parameters.Add(new SqlParameter("@discount", sale.discount));
                cmd.Parameters.Add(new SqlParameter("@category", sale.category));
                con.Open();
                affect = cmd.ExecuteNonQuery();
                con.Close();
                 return affect;

            }
        }
        public int saveToInst(Installment inst)
        {
            int affect = 0;
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("InstallmentTableInsert", con);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add(new SqlParameter("@date", inst.date));
                cmd.Parameters.Add(new SqlParameter("@property_id", inst.property_id));
                cmd.Parameters.Add(new SqlParameter("@term", inst.term));
                cmd.Parameters.Add(new SqlParameter("@project_id", inst.project_id));
                cmd.Parameters.Add(new SqlParameter("@sector_id", inst.sector_id));
                cmd.Parameters.Add(new SqlParameter("@street_no", inst.street_no));
                cmd.Parameters.Add(new SqlParameter("@house_no", inst.house_no));
                cmd.Parameters.Add(new SqlParameter("@length", inst.length));
                cmd.Parameters.Add(new SqlParameter("@width", inst.width));
                cmd.Parameters.Add(new SqlParameter("@marla", inst.marla));
                cmd.Parameters.Add(new SqlParameter("@covered_area", inst.covered_area));
                cmd.Parameters.Add(new SqlParameter("@rate", inst.rate));
                cmd.Parameters.Add(new SqlParameter("@rate_criteria", inst.rate_criteria));
                cmd.Parameters.Add(new SqlParameter("@total_amount", inst.total_amount));
                cmd.Parameters.Add(new SqlParameter("@total_cost", inst.total_cost));
                cmd.Parameters.Add(new SqlParameter("@advance", inst.advance));
                cmd.Parameters.Add(new SqlParameter("@difference", inst.difference));
                cmd.Parameters.Add(new SqlParameter("@payment_mode", inst.payment_mode));
                cmd.Parameters.Add(new SqlParameter("@name", inst.name));
                cmd.Parameters.Add(new SqlParameter("@f_name", inst.f_name));
                cmd.Parameters.Add(new SqlParameter("@cnic", inst.cnic));
                cmd.Parameters.Add(new SqlParameter("@address", inst.address));
                cmd.Parameters.Add(new SqlParameter("@p_address", inst.p_address));
                cmd.Parameters.Add(new SqlParameter("@mobile", inst.mobile));
                cmd.Parameters.Add(new SqlParameter("@ph_reserve", (inst.ph_reserve == null) ? "": inst.ph_reserve));
                cmd.Parameters.Add(new SqlParameter("@ph_office", (inst.ph_office == null)? "" : inst.ph_office));
                cmd.Parameters.Add(new SqlParameter("@fax", (inst.fax == null)? "" : inst.fax));
                cmd.Parameters.Add(new SqlParameter("@total_installments", inst.total_installments));
                cmd.Parameters.Add(new SqlParameter("@category", inst.category));
                cmd.Parameters.Add(new SqlParameter("@per_installment", inst.per_installment));
                cmd.Parameters.Add(new SqlParameter("@image", inst.image));
                con.Open();
                affect = cmd.ExecuteNonQuery();
                con.Close();
                return affect;

            }
        }
    }
}