﻿using Property_Sale_Project.Models.Project;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace Property_Sale_Project.BusinessModel.Add_ProjectModel
{
    public class ProjectBusinessLayer
    {
        private string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;

        public List<Project> fetchAll()
        {
            List<Project> projects = new List<Project>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select * from projects", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Project project = new Project();
                    project.project_id = Convert.ToInt32(reader["project_id"]);
                    project.project_name = reader["project_name"].ToString();
                    project.description = reader["description"].ToString();

                    projects.Add(project);
                }
                con.Close();
            }
            return projects;
        }
        public int getMaxid()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                int id = -1;
                SqlCommand cmd = new SqlCommand("Select max(project_id)+1 as project_id from projects", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    id = Convert.ToInt32(reader["project_id"]);
                }
                return id;
            }
        }
        public Project fetchEdit(int id)
        {
            Project project = new Project();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select * from projects where project_id = "+ id +"", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    project.project_id = Convert.ToInt32(reader["project_id"]);
                    project.project_name = reader["project_name"].ToString();
                    project.description = reader["description"].ToString();
                }
                con.Close();
                return project;
            }
            
        }
        public int save(Project data)
        {
            if (data.project_id.Equals(0))
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    SqlCommand cmd = new SqlCommand("insert_project", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@p_name", data.project_name));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.description));
                    con.Open();
                    int n = cmd.ExecuteNonQuery();
                    con.Close();
                    return n;

                }

            }
            else
            {
                using (SqlConnection con = new SqlConnection(constr))
                {
                    SqlCommand cmd = new SqlCommand("update_project", con);
                    cmd.CommandType = System.Data.CommandType.StoredProcedure;
                    cmd.Parameters.Add(new SqlParameter("@p_id", data.project_id));
                    cmd.Parameters.Add(new SqlParameter("@p_name", data.project_name));
                    cmd.Parameters.Add(new SqlParameter("@desc", data.description));
                    con.Open();
                    int n = cmd.ExecuteNonQuery();
                    con.Close();
                    return n;

                }

            }
        }
        
    }
}