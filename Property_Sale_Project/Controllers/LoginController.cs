﻿using Property_Sale_Project.BusinessModel;
using Property_Sale_Project.Models.User;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/
        private string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;

        //here is the connection string that connect with Db automatically
        // View sy data controller me pass krwany atta??  NAI
        //mujy ajax use kr k data pass karway ata asp.net main 
        //razor view tmhain ata hai 


        //this function returns the login form
        //user table khty aa or yaha project me kha hn ??
        public ActionResult Index()
        {
            
            return View();
        }
        public ActionResult Mylogin()
        {
       
            return View();
        }
        [HttpPost] // aa la  k zriya data ly aa jasy la sakty ho
        public ActionResult Mylogin(MyUser usermodel)
        {
            
            MySession.Current.USerID = 0;  //boss yaha py 1 ki jagha user jo login ho oski id aani chiye baki code sai hn  
            MyUser user = new MyUser();
            using (SqlConnection con = new SqlConnection(constr))
            {
                // qury tmhri thk hn
                SqlCommand cmd = new SqlCommand("select * from users where name = '" + usermodel.name + "' and password='" + usermodel.password + "' ", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    user.name = reader["name"].ToString();
                    user.email = reader["email"].ToString();
                    MySession.Current.USerID = Convert.ToInt32(reader["id"]);

                    con.Close();
                    return RedirectToAction("Index", "Dashboard");
                }
                else
                {
                    con.Close();
                    ViewBag.error = "User Not Found!";
                }
                return View();
               
            }
            
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            Session.Clear();
            return RedirectToAction("Mylogin","Login");
        }

    }
}
