﻿
using Property_Sale_Project.BusinessModel.ResaleModel;
using Property_Sale_Project.Models.InstallmentRecipt;
using Property_Sale_Project.Models.Sale_Property;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class ResaleController : RouterController
    {
        //
        // GET: /Resale/

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult fetchProperty(int id)
        {
            string status = "";
            ResalePropertyBusinessModel rbm = new ResalePropertyBusinessModel();
            Installment_plan inst = rbm.fetchInst(id);
            status = "Under Installments";
            object[] send_sale = new object[2];
            send_sale[0] = status;
            send_sale[1] = inst;
            return Json(send_sale);


        }
    }
}
