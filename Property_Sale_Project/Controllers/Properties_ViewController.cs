﻿using Microsoft.Reporting.WebForms;
using Property_Sale_Project.BusinessModel.Add_PropertyModel;
using Property_Sale_Project.Models.AddProperty;
//using Property_Sale_Project.Models.Sale_Property;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class Properties_ViewController : RouterController
    {
        private string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;
        //
        // GET: /Properties_View/

        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Report(string id)
        {
            LocalReport lr = new LocalReport();
            string path = Path.Combine(Server.MapPath("~/Report"), "MyRreport.rdlc");
            if (System.IO.File.Exists(path))
            {
                lr.ReportPath = path;
            }
            else
            {
                return View("Index");
            }
            List<Property_Sale_Project.Models.Sale_Property.Property> propertylist = new List<Property_Sale_Project.Models.Sale_Property.Property>();
            using (SqlConnection con = new SqlConnection(constr))
            {
                SqlCommand cmd = new SqlCommand("Select p.property_id, p.category, p.length, p.width, p.marla, p.covered_area, p.rate, p.amount, p.total_cost,p.total_amount, p.status, pr.project_name, sec.sector_name from Properties p join Projects pr on(p.project_id = pr.project_id) join sector sec on(p.sector_id = sec.sector_id)", con);
                con.Open();
                cmd.ExecuteNonQuery();
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    Property_Sale_Project.Models.Sale_Property.Property property = new Property_Sale_Project.Models.Sale_Property.Property();
                    property.property_id = Convert.ToInt32(reader["property_id"]);
                    property.project_name = reader["project_name"].ToString();
                    property.sector_name = reader["sector_name"].ToString();
                    property.category = reader["category"].ToString();
                    property.length = Convert.ToDouble(reader["length"]);
                    property.width = Convert.ToDouble(reader["width"]);
                    property.marla = Convert.ToDouble(reader["marla"]);
                    property.covered_area = Convert.ToDouble(reader["covered_area"]);
                    property.rate = Convert.ToDouble(reader["rate"]);
                    property.amount = Convert.ToDouble(reader["amount"]);
                    property.total_cost = Convert.ToDouble(reader["total_cost"]);
                    property.total_amount = Convert.ToDouble(reader["total_amount"]);
                    property.status = reader["status"].ToString();
                    propertylist.Add(property);
                }
            }
            // yr  yha 1 extension aye gi wndow.repost.web es trha ki ho gi tu b check kr 
            ReportDataSource rd = new ReportDataSource("DataSetP", propertylist);
            lr.DataSources.Add(rd);
            string reportType = id;
            string mimeType;
            string encoding;
            string fileNameExtension;



            string deviceInfo =

            "<DeviceInfo>" +
            "  <OutputFormat>" + id + "</OutputFormat>" +
            "  <PageWidth>8.5in</PageWidth>" +
            "  <PageHeight>11in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>1in</MarginLeft>" +
            "  <MarginRight>1in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            renderedBytes = lr.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);


            return File(renderedBytes, mimeType);
        }
        [HttpPost]
        public JsonResult getAllProperties()
        {
            PropertiesBusinessModel pbm = new PropertiesBusinessModel();
            List<Property> properties = pbm.getAllProperties();
            return Json(properties, JsonRequestBehavior.AllowGet);
        }

    }
}
