﻿using Property_Sale_Project.BusinessModel;
using Property_Sale_Project.BusinessModel.Installment_Recipt_VoucherModel;
using Property_Sale_Project.Models.InstallmentRecipt;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class Installment_Recipt_VoucherController : RouterController
    {
        //
        // GET: /InatallmentReciptVoucher/

        public ActionResult installment_Recipt_Voucher()
        {
            var res = MySession.Current.USerID;
            if (res == 0)
            {
                return RedirectToAction("Mylogin", "Login");
            }
            else
            {

                return View();
            }
        }
        [HttpPost]
        public JsonResult getMaxId()
        {
            Inst_reciptBusinessModel ibm = new Inst_reciptBusinessModel();
            int id = ibm.getMaxId();
            return Json(id);
        }
        [HttpPost]
        public JsonResult fetchPro(int pid)
        {
            Inst_reciptBusinessModel ibm = new Inst_reciptBusinessModel();
            Installment_plan inst = ibm.fetchPropertyInstPlan(pid);
            if (inst.property_id.Equals(0))
            {
                Installment installment = new Installment();
                installment = ibm.fetchPropertyInst(pid);
                return Json(installment);
            }
            else
            {
                return Json(inst);
            }
        }
        [HttpPost]
        public JsonResult fetchDiff(int pid)
        {
            Inst_reciptBusinessModel ibm = new Inst_reciptBusinessModel();
            double difference = ibm.diffInstPlan(pid);
            if (difference.Equals(0))
            {
                difference = ibm.diffInst(pid);
                return Json(difference);
            }
            else
            {
                return Json(difference);
            }
        }
        [HttpPost]
        public JsonResult fetchTableDetail(int pid)
        {
            Inst_reciptBusinessModel ibm = new Inst_reciptBusinessModel();
            List<Installment_plan> instPlan = ibm.fetchInstPlanDetail(pid);
            return Json(instPlan);
        }
        [HttpPost]
        public JsonResult saveInstToPlan(Installment_plan plan)
        {
            Inst_reciptBusinessModel ibm = new Inst_reciptBusinessModel();
            int affect = ibm.insertToInstPlan(plan);
            return Json(affect);
        }
    }
}
