﻿using Property_Sale_Project.BusinessModel.Sale_PropertyModel;
using Property_Sale_Project.Models.Sale_Property;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Property_Sale_Project.Models.InstallmentRecipt;
using Property_Sale_Project.Models.Mode;
using System.IO;

namespace Property_Sale_Project.Controllers
{
    public class Sale_PropertyController : RouterController
    {
        //
        // GET: /Sale_Property/

        public ActionResult sale_Property()
        {
            return View();
        }
        [HttpPost]
        public JsonResult getMaxId()
        {
            Sale_PropertyBusinessModel sbm = new Sale_PropertyBusinessModel();
            int id = sbm.getMaxId();
            return Json(id);
        }
        [HttpPost]
        public JsonResult fetchProperty(int id)
        {
            //agar data sale_Property table main hai tu soled show ho jaye.
            //agar data installment table main hai tu under installment show ho jaye.
            //agar dono main data na ho to property table say data pick ker lay.
            string status = "";
            Sale_PropertyBusinessModel sbm = new Sale_PropertyBusinessModel();
            Property sale = sbm.fetchSale(id);
            if (sale.property_id.Equals(0))
            {
                Property inst = sbm.fetchInst(id);
                if (inst.property_id.Equals(0))
                {
                    Property property = sbm.fetchPro(id);
                    status = "Available";
                    object[] send_pro = new object[2];
                    send_pro[0] = status;
                    send_pro[1] = property;
                    return Json(send_pro);
                }
                status = "Under Installment";
                object[] send_inst = new object[2];
                send_inst[0] = status;
                send_inst[1] = inst;
                return Json(send_inst);
            }
            status = "Soled Property";
            object[] send_sale = new object[2];
            send_sale[0] = status;
            send_sale[1] = sale;
            return Json(send_sale);
        }
        [HttpPost]
        public JsonResult saveToSale(Sale_Property sale)
        {

            Sale_PropertyBusinessModel sbm = new Sale_PropertyBusinessModel();
            int affect = sbm.saveToSale(sale);
            return Json(affect);

        }
        [HttpPost]
        public JsonResult saveToInst(Installment Inst)
        {

            Sale_PropertyBusinessModel sbm = new Sale_PropertyBusinessModel();
            int affect = sbm.saveToInst(Inst);
            return Json(affect);
        }
        [HttpPost]
        public JsonResult saveToDirectory()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/images"), fileName);
                file.SaveAs(path);
                return Json(1);
            }
            catch (Exception e)
            {
                return Json(e);
            }
        }
        [HttpPost]
        public JsonResult fillPayMode()
        {
            Sale_PropertyBusinessModel sbm = new Sale_PropertyBusinessModel();
            List<Mode> mode = sbm.fetchPayMode();
            return Json(mode);
        }

    }
}



//if (file.ContentLength > 0)
//            {
//                var fileName = Path.GetFileName(file.FileName);
//                var path = Path.Combine(Server.MapPath("~/images"), fileName);
//                file.SaveAs(path);
//            }


// if (file.ContentLength > 0)
//            {
//                var fileName = Path.GetFileName(file.FileName);
//                var path = Path.Combine(Server.MapPath("~/images"), fileName);
//                file.SaveAs(path);
//            }