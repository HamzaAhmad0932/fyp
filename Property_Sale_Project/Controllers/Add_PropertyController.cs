﻿using Property_Sale_Project.BusinessModel.Add_PropertyModel;
using Property_Sale_Project.Models.AddProperty;
using Property_Sale_Project.Models.Others;
using Property_Sale_Project.Models.Project;
using Property_Sale_Project.Models.Sector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class Add_PropertyController : RouterController
    {
        //
        // GET: /Add_Property/

        public ActionResult index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult getMaxId()
        {
            PropertiesBusinessModel pbm = new PropertiesBusinessModel();
            int id = pbm.getMaxId();
            return Json(id);
        }
        [HttpPost]
        public JsonResult save(Property d)
        {
            PropertiesBusinessModel pbm = new PropertiesBusinessModel();
            int affect = pbm.insert(d);
            return Json(affect);
        }
        [HttpPost]
        public JsonResult fetchProject()
        {
            PropertiesBusinessModel pbl = new PropertiesBusinessModel();
            List<Project> projects = pbl.fetchProject();
            return Json(projects);
        }
        [HttpPost]
        public JsonResult fetchSector()
        {
            PropertiesBusinessModel pbl = new PropertiesBusinessModel();
            List<Sector> sectors = pbl.fetchSector();
            return Json(sectors);
        }
        public JsonResult building()
        {
            PropertiesBusinessModel pbl = new PropertiesBusinessModel();
            Other other = pbl.building();
            return Json(other);
        }
    }
}
