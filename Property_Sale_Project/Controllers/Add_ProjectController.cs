﻿using Property_Sale_Project.BusinessModel.Add_ProjectModel;
using Property_Sale_Project.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class Add_ProjectController : RouterController
    {
        //
        // GET: /Add_Project/

        public ActionResult add_Project()
        {
            return View();
        }
        [HttpPost]
        public JsonResult fetchTable()
        {
            ProjectBusinessLayer pbl = new ProjectBusinessLayer();
            List<Project> projects = pbl.fetchAll();
            return Json(projects);
        }
        [HttpPost]
        public JsonResult getMaxId()
        {
            ProjectBusinessLayer pbl = new ProjectBusinessLayer();
            int data = pbl.getMaxid();
            return Json(data);
        }
        [HttpPost]
        public JsonResult fetchEdit(int id)
        {
            ProjectBusinessLayer pbl = new ProjectBusinessLayer();
            Project data = pbl.fetchEdit(id);
            return Json(data);
        }
        [HttpPost]
        public JsonResult insert(Project d)
        {
            ProjectBusinessLayer pbl = new ProjectBusinessLayer();
            int affect = pbl.save(d);
            return Json(affect);
        }
    }
}
