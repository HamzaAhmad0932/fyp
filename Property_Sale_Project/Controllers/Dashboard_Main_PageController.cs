﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace Property_Sale_Project.Controllers
{
    public class Dashboard_Main_PageController : RouterController
    {
        //
        // GET: /Dashboard_Main_Page/
        private string constr = ConfigurationManager.ConnectionStrings["Property_Sale"].ConnectionString;


        private static float totalper;
        private static double installmentper;
        private static double saleper;
        private static double availableper;


        public ActionResult Index()
        {
            using (SqlConnection con = new SqlConnection(constr))
            {
                int total = 0;
                int sale = 0;
                int installment = 0;
                int available = 0;
                SqlCommand cmd1 = new SqlCommand("select count(*) as 'total' from Properties", con);
                con.Open();
                cmd1.ExecuteNonQuery();
                SqlDataReader reader1 = cmd1.ExecuteReader();
                if (reader1.Read())
                {
                    if (reader1["total"].ToString() != "")
                    {
                        total = Convert.ToInt32(reader1["total"]);
                    }

                }
                con.Close();
                SqlCommand cmd2 = new SqlCommand("select count(*) as 'installment' from installments", con);
                con.Open();
                cmd2.ExecuteNonQuery();
                SqlDataReader reader2 = cmd2.ExecuteReader();
                if (reader2.Read())
                {
                    
                        installment = Convert.ToInt32(reader2["installment"]);
                    

                }
                con.Close();

                SqlCommand cmd3 = new SqlCommand("select count(*) as 'sale' from sale_property", con);
                con.Open();
                cmd3.ExecuteNonQuery();
                SqlDataReader reader3 = cmd3.ExecuteReader();
                if (reader3.Read())
                {
                    
                        sale = Convert.ToInt32(reader3["sale"]);
                    

                }
                con.Close();

                SqlCommand cmd4 = new SqlCommand("select count(*) as 'available' from Properties where status = 'Available'", con);
                con.Open();
                cmd4.ExecuteNonQuery();
                SqlDataReader reader4 = cmd4.ExecuteReader();
                if (reader4.Read())
                {

                    available = Convert.ToInt32(reader4["available"]);


                }
                con.Close();

                 totalper = (total / total) * 100;
                 installmentper = (double)(installment * 100 ) / total;
                 saleper = (double)(sale * 100) / total;
                 availableper = (double)(available * 100) / total;

                ViewBag.total = total;
                ViewBag.installment = installment;
                ViewBag.sale = sale;
                ViewBag.available = available;

                ViewBag.totalper = totalper;
                ViewBag.installmentper = Math.Round(installmentper, 1);
                ViewBag.saleper = Math.Round(saleper, 1);
                ViewBag.availableper = Math.Round(availableper, 1);
                return View();
            }
        }

        [HttpPost]
        public JsonResult NewChart()
        {
            List<object> iData = new List<object>();
            //Creating sample data  
            DataTable dt = new DataTable();
            dt.Columns.Add("Employee", System.Type.GetType("System.String"));
            dt.Columns.Add("Credit", System.Type.GetType("System.Double"));

            DataRow dr = dt.NewRow();

            dr = dt.NewRow();
            dr["Employee"] = "Installment";
            dr["Credit"] = Math.Round(installmentper, 1);
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Employee"] = "Sale";
            dr["Credit"] = Math.Round(saleper, 1);
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["Employee"] = "Available";
            dr["Credit"] = Math.Round(availableper, 1);
            dt.Rows.Add(dr);

            //Looping and extracting each DataColumn to List<Object>  
            foreach (DataColumn dc in dt.Columns)
            {
                List<object> x = new List<object>();
                x = (from DataRow drr in dt.Rows select drr[dc.ColumnName]).ToList();
                iData.Add(x);
            }
            //Source data returned as JSON  
            return Json(iData, JsonRequestBehavior.AllowGet);
        } 

    }
}



            //DataRow dr = dt.NewRow();
            //dr["Employee"] = "Total";
            //dr["Credit"] = totalper;
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            //dr["Employee"] = "Installment";
            //dr["Credit"] = installmentper;
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            //dr["Employee"] = "Sale";
            //dr["Credit"] = saleper;
            //dt.Rows.Add(dr);

            //dr = dt.NewRow();
            //dr["Employee"] = "Available";
            //dr["Credit"] = availableper;
            //dt.Rows.Add(dr);