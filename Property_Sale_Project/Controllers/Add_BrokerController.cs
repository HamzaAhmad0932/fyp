﻿using Property_Sale_Project.BusinessModel.Add_BrokerController;
using Property_Sale_Project.Models.Broker;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Property_Sale_Project.Controllers
{
    public class Add_BrokerController : RouterController
    {
        //
        // GET: /Add_Broker/

        public ActionResult add_Broker()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetMaxId()
        {
            BrokerBusinessLayer bbl = new BrokerBusinessLayer();
            int id = bbl.getMaxId();
            return Json(id);
        }
        [HttpPost]
        public JsonResult fetchAll()
        {
            BrokerBusinessLayer bbl = new BrokerBusinessLayer();
            List<Broker> brokers = bbl.fetchAll();
            return Json(brokers);

        }
        [HttpPost]
        public JsonResult fetchEdit(int id)
        {
            BrokerBusinessLayer bbl = new BrokerBusinessLayer();
            Broker broker = bbl.fetchedit(id);
            return Json(broker);
        }
        [HttpPost]
        public JsonResult save(Broker broker)
        {
            BrokerBusinessLayer bbl = new BrokerBusinessLayer();
            int affect = bbl.save_broker(broker);
            return Json(affect);
        }

    }
}
